/**
 * add a comment here TBI
 */

#ifndef FLOWWITHMULTIPARTICLECORRELATIONS_H
#define FLOWWITHMULTIPARTICLECORRELATIONS_H

#include <TList.h>
#include <Riostream.h>
#include <TSystem.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TF1.h>
#include <TComplex.h>
#include <TProfile.h>
#include <TRandom3.h>
using namespace std;

// VK:
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/DataHeader.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/Detector.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/EventHeader.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/TreeReader.h"
using namespace AnalysisTree;

class FlowWithMultiparticleCorrelations{

 // a) Basic structure;
 // b) Q-vectors;
 // c) Setters and getters;
 // d) Data members;

 public:  
  FlowWithMultiparticleCorrelations(const char *name);
  FlowWithMultiparticleCorrelations(); // dummy constructor
  virtual ~FlowWithMultiparticleCorrelations(); 
 
  // a) Basic structure:BookAndNestAllLists
  virtual void InitializeArrays(); 
  virtual void DefaultEventCuts(); 
  virtual void BookEverything(); // book all histograms, etc.
    virtual void BookAndNestAllLists(); // book all lists
    virtual void BookAllProfilesHoldingFlags(); // bool all permanent profiles here 
    virtual void BookAllOnTheFlyObjects(); // book all objects for Toy Monte Carlo (flow analysis 'on-the-fly') here
  virtual void ProcessEvents(); // for each event only this method is executed
    virtual void CalculateOnTheFly(void);
    virtual void CalculateCorrelations(void);
    virtual void CalculateNestedLoops(void);
    virtual void ResetEventByEventQuantities(void);
  virtual void Terminate(); // called only after all events are processed
    virtual void ComparisonNestedLoopsVsCorrelations(void);

  // b) Q-vectors:
  virtual Float_t Weight(const Float_t &value, const char *variable); // TBI_20200612 perhaps this one shall be sorted better in a different category
  virtual TComplex Q(Int_t n, Int_t p);
  virtual TComplex One(Int_t n1);
  virtual TComplex Two(Int_t n1, Int_t n2);
  virtual TComplex Three(Int_t n1, Int_t n2, Int_t n3);
  virtual TComplex Four(Int_t n1, Int_t n2, Int_t n3, Int_t n4);
  virtual TComplex Five(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5);
  virtual TComplex Six(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6);
  virtual TComplex Seven(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7);
  virtual TComplex Eight(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7, Int_t n8);
  virtual TComplex Recursion(Int_t n, Int_t* harmonic, Int_t mult = 1, Int_t skip = 0); // Credits: Kristjan Gulbrandsen (gulbrand@nbi.dk) 

  // c) Setters and getters:
  void SetInputData(const char *ra, const char *id)
  {
   if(!(TString(ra).EqualTo("reco") || TString(ra).EqualTo("analysis")))
   {
    cout<<"FATAL: Only \"reco\" or \"analysis\" are supported for the 1st argument in this setter."<<endl; exit(0);
   }
   if(TString(ra).EqualTo("reco")){this->fInputData[0] = TString(id);}
   if(TString(ra).EqualTo("analysis")){this->fInputData[1] = TString(id);}
  }
  void SetMaxNoFiles(const Int_t mnf) {this->fMaxNoFiles = mnf;};
  Int_t GetMaxNoFiles() const {return this->fMaxNoFiles;};
  void SetOutputFileName(const char *ofn) {this->fOutputFileName = TString(ofn);};
  TString GetOutputFileName() const {return this->fOutputFileName;};
  void SetFillControlHistogramsEvents(const Bool_t before, const Bool_t after)
  {
   this->fFillControlHistogramsEvents[0] = before; // fill histos before cuts
   this->fFillControlHistogramsEvents[1] = after;  // fill histos after cuts
  }
  void SetFillControlHistogramsParticles(const Bool_t before, const Bool_t after)
  {
   this->fFillControlHistogramsParticles[0] = before; // fill histos before cuts
   this->fFillControlHistogramsParticles[1] = after;  // fill histos after cuts
  }
  void SetMultiplicityCuts(const Float_t min, const Float_t max)
  {
   this->fMultiplicityCuts[0] = min;
   this->fMultiplicityCuts[1] = max;
  }

  void SetVertexCuts(const char* sxyz, const Float_t min, const Float_t max)
  {
   Int_t xyz = -44;
   if(TString(sxyz).EqualTo("x"))  
   {
    xyz = 0; 
   } 
   else if (TString(sxyz).EqualTo("y"))
   {
    xyz = 1; 
   } 
   else if (TString(sxyz).EqualTo("z"))
   {
    xyz = 2;
   }
   else
   {
    cout<<"FATAL: Only \"x\", \"y\" or \"z\" are supported for the 1st argument in this setter."<<endl; exit(0);
   } // if("x" == xyz)
   this->fVertexCuts[xyz][0] = min;
   this->fVertexCuts[xyz][1] = max;
  }
 
 void SetCalculateQvector(Bool_t cqv) {this->fCalculateQvector = cqv;};
 Bool_t GetCalculateQvector() const {return this->fCalculateQvector;};

 void SetCalculateCorrelations(Bool_t cc) {this->fCalculateCorrelations = cc;};
 Bool_t GetCalculateCorrelations() const {return this->fCalculateCorrelations;};

 void SetCalculateNestedLoops(Bool_t cnl) {this->fCalculateNestedLoops = cnl;};
 Bool_t GetCalculateNestedLoops() const {return this->fCalculateNestedLoops;};
 void SetCalculateOnTheFly(Bool_t cotf, Int_t noe = 100) 
 {
  this->fCalculateOnTheFly = cotf;
  this->fnEvents = noe;
 };
 void SetRandomSeed(UInt_t rs) {this->fRandomSeed = rs;}; 
 Bool_t GetNonDefaultPDFs(const char *filePath);
 void SetMass(Float_t dMass) {this->fMass = dMass;}
 void SetTemperature(Float_t dT) {this->fTemperature = dT;}
 void SetV1(Float_t dV1) {this->fV1 = dV1;}
 void SetV2(Float_t dV2) {this->fV2 = dV2;}
 void SetV3(Float_t dV3) {this->fV3 = dV3;}
 void SetV4(Float_t dV4) {this->fV4 = dV4;}
 void SetV5(Float_t dV5) {this->fV5 = dV5;}
 void SetV6(Float_t dV6) {this->fV6 = dV6;}
 Bool_t AcceptPhi(const Float_t &value);  
 void SetProbabilities(Float_t dP1, Float_t dP2) // probabilities that particle is reconstructed in [-3Pi/4,Pi/4] and [Pi/3,2Pi/3], respectively 
 {
  this->fProbability[0] = dP1;
  this->fProbability[1] = dP2;
 };
 void SetMultiplicityRange(Float_t min, Float_t max) // min and max values for uniform fMultiplicityPDF in on-the-fly analysis
 {
  this->fMultRangePDF[0] = min;
  this->fMultRangePDF[1] = max;
 };

 void SetWeightsHist(TH1F* const hist, const char *variable); // .cxx
 TH1F* GetHistogramWithWeights(const char *filePath, const char *variable); // .cxx

 private:
  FlowWithMultiparticleCorrelations(const FlowWithMultiparticleCorrelations& fwmc);
  FlowWithMultiparticleCorrelations& operator=(const FlowWithMultiparticleCorrelations& fwmc);
 
  // d) Data members:
  //  d0) Grandmother of all lists:
  TList *fHistList; // base list to hold all output object

  //  d1) Control histograms: 
  TList *fControlHistogramsList; // base list to hold all control histograms
  TProfile *fControlHistogramsPro; // keeps all flags for control histograms
  // Global event observables: 
  TList *fControlHistogramsEventsList; // list to hold all histograms for global event quantities
  TProfile *fControlHistogramsEventsPro; // keeps all flags for event observalbes
  Bool_t fFillControlHistogramsEvents[2]; // fill control histograms for events [before,after] cuts
  TH1F *fMultiplicity[2]; // multiplicity distribution [before,after] the cuts  
  Float_t fMultiplicityCuts[2]; // [min,max] multiplicity 
  Int_t fnSelectedTracksEBE; // number of selected tracks for the analysis in the current event 
  TH1F *fImpactParameter[2][2]; // orientation of impact parameter [before,after][reco,sim]
  TH1F *fVertex[2][2][3]; // vertex distribution [before,after][reco,sim][x,y,z] 
  Float_t fVertexCuts[3][2]; // [x,y,z][min,max] vertex components  
  // Particle distributions:
  TList *fControlHistogramsParticlesList; // list to hold all control histograms with particle distributions
  TProfile *fControlHistogramsParticlesPro; // keeps all flags for particle distributions
  Bool_t fFillControlHistogramsParticles[2]; // fill control histograms for particles [before,after] cuts
  TH1F *fDCA[2][3][3]; // distance of closest approach (DCA) [before,after][reco,reco+sim,sim][x,y,z]
  TH1F *fKinematics[2][3][5]; // kinematics [before,after][reco,reco+sim,sim][phi,pt,eta,p,rapidity]
  TH1F *fPID[2][3][1]; // PID info [before,after][reco,reco+sim,sim][PDG]

  //  d2) Input dataset: 
  TString fInputData[2]; // path to ASCII files holding all input files which will be processed ["reco","analysis"]
  Int_t fMaxNoFiles;  // maximum number of files to process, -1 means all (default)   

  //  d3) Particle weights:
  TList *fWeightsList;        // list to hold all weights objects
  TProfile *fWeightsPro;      // profile to hold all flags for weights
  Bool_t fUseWeights[4];      // use weights [phi,pt,eta,rapidity]
  TH1F *fWeightsHist[4];      // histograms holding weights [phi,pt,eta,rapidity]

  //  d4) Q-vectors:
  TList *fQvectorList;        // list to hold all Q-vector objects       
  TProfile *fQvectorFlagsPro; // profile to hold all flags for Q-vector
  Bool_t fCalculateQvector;   // to calculate or not to calculate Q-vector components, that's a Boolean...
  Int_t fMaxHarmonic;         // 6 (not going beyond v6, if you change this value, change also fQvector[49][9]) 
  Int_t fMaxCorrelator;       // 8 (not going beyond 8-p correlations, if you change this value, change also fQvector[49][9]) 
  TComplex fQvector[49][9];   // Q-vector components [fMaxHarmonic*fMaxCorrelator+1][fMaxCorrelator+1] = [6*8+1][8+1]  

  //  d5) Multiparticle correlations:
  TList *fCorrelationsList;               // list to hold all correlations objects
  TProfile *fCorrelationsFlagsPro;        // profile to hold all flags for correlations
  Bool_t fCalculateCorrelations;          // calculate and store correlations
  TProfile *fCorrelationsPro[2][4][6][3]; // multiparticle correlations [0=cos,1=sin][2p=0,4p=1,6p=2,8p=3][n=1,n=2,...,n=6][0=integrated,1=vs. multiplicity,2=vs. centrality]

  //  d6) Cross-check with nested loops:
  TList *fNestedLoopsList;               // list to hold all nested loops objects
  TProfile *fNestedLoopsFlagsPro;        // profile to hold all flags for nested loops
  Bool_t fCalculateNestedLoops;          // calculate and store correlations with nested loops, as a cross-check
  TProfile *fNestedLoopsPro[2][4][6][3]; // multiparticle correlations from nested loops [0=cos,1=sin][2p=0,4p=1,6p=2,8p=3][n=1,n=2,...,n=6][0=integrated,1=vs. multiplicity,2=vs. centrality]
  TArrayF *ftaNestedLoops[2];            // event container for nested loops [0=angles;1=weights]   

  //  d7) Toy Monte Carlo (flow analysis 'on-the-fly'):
  TList *fOnTheFlyList;        // list to hold all 'on-the-fly' objects
  TProfile *fOnTheFlyFlagsPro; // profile to hold all flags for nested loops
  Bool_t fCalculateOnTheFly;   // calculate and store correlations with nested loops, as a cross-check
  Bool_t fUseDefaultPDFs;      // kTRUE: use default hardwired distributions for multiplicity, pt, phi and eta
                               // kFALSE: supply via setter and histogram the distributions for multiplicity, pt, phi and eta   
  TF1 *fPtPDF;                 // default transverse momentum distribution (pt is sampled from hardwired Boltzmann distribution)
  Float_t fMass;               // mass in pt distribution (hardwired is Boltzmann pt distribution)
  Float_t fTemperature;        // "temperature" in pt distribution (hardwired is Boltzmann pt distribution)   
  TF1 *fPhiPDF;                // azimuthal distribution (phi is sampled from hardwired Fourier-like distribution)
  Float_t fV1;                 // harmonic v1 
  Float_t fV2;                 // harmonic v2
  Float_t fV3;                 // harmonic v3
  Float_t fV4;                 // harmonic v4
  Float_t fV5;                 // harmonic v5
  Float_t fV6;                 // harmonic v6
  TF1 *fEtaPDF;                // default eta distribution
  TF1 *fMultiplicityPDF;       // default multiplicity distribution is uniform, from fMultRangePDF[0] to fMultRangePDF[1]
  Float_t fMultRangePDF[2];    // min and max values for uniform fMultiplicityPDF in on-the-fly analysis
  TF1 *fDetectorAcceptancePDF; // default detector acceptance profile
  Float_t fProbability[2];     // [0 = p that particle is reconstructed in [-3Pi/4,Pi/4], 1 = p that particle is reconstructed in [Pi/3,2Pi/3]] 
  UInt_t fRandomSeed;          // if fRandomSeed is 0 (by default yes), the seed is determined uniquely in space and time via TUUID
  Int_t fnEvents;              // how many Toy Monte Carlo events
  TH1F *fNonDefaultPDFs[4];    // [0=multiplicity,1=pt,2=eta,3=detector's azimuthal acceptance]

  //  ??) Final output file name:
  TString fOutputFileName; // the name of the final output file, defaulted to "AnalysisResults.root"

 ClassDef(FlowWithMultiparticleCorrelations,1);

};

#endif

