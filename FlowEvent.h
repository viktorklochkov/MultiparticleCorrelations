/**
 * add a comment here TBI
 */

#ifndef FLOWEVENT_H
#define FLOWEVENT_H

#include "TArrayF.h"
#include "Riostream.h"

// VK:
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/DataHeader.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/Detector.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/EventHeader.h"
#include "/home/abilandz/CBM/AnalysisTree/20200217/analysis_tree-master/src/TreeReader.h"
using namespace AnalysisTree;

class FlowEvent{

 public:  
  FlowEvent(const char *name);
  FlowEvent(); // dummy constructor
  virtual ~FlowEvent();   

 private:
  FlowEvent(const FlowEvent& fe);
  FlowEvent& operator=(const FlowEvent& fe);

  TArrayF *fAngles; // base list to hold all output object

  ClassDef(FlowEvent,1);

};

#endif

