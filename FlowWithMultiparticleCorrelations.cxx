/**
 * add a comment here TBI
 */

#include "FlowWithMultiparticleCorrelations.h" 

ClassImp(FlowWithMultiparticleCorrelations)

//=======================================================================================

FlowWithMultiparticleCorrelations::FlowWithMultiparticleCorrelations(const char *name): 
 fHistList(NULL),
 fControlHistogramsList(NULL),
 fControlHistogramsPro(NULL),
 fControlHistogramsEventsList(NULL),
 fControlHistogramsEventsPro(NULL),
 fnSelectedTracksEBE(0), 
 fControlHistogramsParticlesList(NULL),
 fControlHistogramsParticlesPro(NULL),
 fWeightsList(NULL),
 fWeightsPro(NULL),
 fQvectorList(NULL),
 fQvectorFlagsPro(NULL),
 fCalculateQvector(kTRUE),
 fMaxHarmonic(6),
 fMaxCorrelator(8),
 fCorrelationsList(NULL),        
 fCorrelationsFlagsPro(NULL), 
 fCalculateCorrelations(kTRUE),
 fNestedLoopsList(NULL),        
 fNestedLoopsFlagsPro(NULL), 
 fCalculateNestedLoops(kFALSE),
 fOnTheFlyList(NULL),
 fOnTheFlyFlagsPro(NULL),
 fCalculateOnTheFly(kFALSE),
 fUseDefaultPDFs(kTRUE),
 fPtPDF(NULL),
 fMass(0.13957),
 fTemperature(0.44),
 fPhiPDF(NULL),
 fV1(0.),
 fV2(0.05),
 fV3(0.),
 fV4(0.),
 fV5(0.),
 fV6(0.),
 fEtaPDF(NULL),
 fMultiplicityPDF(NULL),
 fDetectorAcceptancePDF(NULL),
 fRandomSeed(0),
 fnEvents(100),
 fMaxNoFiles(-1),
 fOutputFileName("AnalysisResults.root")
 {
  // Constructor.
 
  // Base list:
  fHistList = new TList();
  fHistList->SetName("MPC"); // multiparticle correlations 
  fHistList->SetOwner(kTRUE);
 
  // Initialize all arrays to NULL;
  this->InitializeArrays();

  // Default cuts:
  this->DefaultEventCuts();

 } // FlowWithMultiparticleCorrelations::FlowWithMultiparticleCorrelations(const char *name): 

//=======================================================================================

FlowWithMultiparticleCorrelations::FlowWithMultiparticleCorrelations(): 
 fHistList(NULL),
 fControlHistogramsList(NULL),
 fControlHistogramsPro(NULL),
 fControlHistogramsEventsList(NULL),
 fControlHistogramsEventsPro(NULL),
 fnSelectedTracksEBE(0), 
 fControlHistogramsParticlesList(NULL),
 fControlHistogramsParticlesPro(NULL),
 fWeightsList(NULL),
 fWeightsPro(NULL),
 fQvectorList(NULL),
 fQvectorFlagsPro(NULL),
 fCalculateQvector(kFALSE),
 fMaxHarmonic(-44),
 fMaxCorrelator(-44),
 fCorrelationsList(NULL),        
 fCorrelationsFlagsPro(NULL), 
 fCalculateCorrelations(kFALSE),
 fNestedLoopsList(NULL),        
 fNestedLoopsFlagsPro(NULL), 
 fCalculateNestedLoops(kFALSE),
 fOnTheFlyList(NULL),
 fOnTheFlyFlagsPro(NULL),
 fCalculateOnTheFly(kFALSE),
 fUseDefaultPDFs(kTRUE),
 fPtPDF(NULL),
 fMass(0.),
 fTemperature(0.),
 fPhiPDF(NULL),
 fV1(0.),
 fV2(0.),
 fV3(0.),
 fV4(0.),
 fV5(0.),
 fV6(0.),
 fEtaPDF(NULL),
 fMultiplicityPDF(NULL),
 fDetectorAcceptancePDF(NULL),
 fRandomSeed(0),
 fnEvents(0),
 fMaxNoFiles(-1),
 fOutputFileName("")
 {
  // Dummy constructor.

 } // FlowWithMultiparticleCorrelations::FlowWithMultiparticleCorrelations():

//=======================================================================================

FlowWithMultiparticleCorrelations::~FlowWithMultiparticleCorrelations()
{
 // Destructor.

 if(fHistList) delete fHistList;

} // FlowWithMultiparticleCorrelations::~FlowWithMultiparticleCorrelations()

//=======================================================================================

void FlowWithMultiparticleCorrelations::BookEverything() 
{
 // Book all histograms, etc.

 // a) Welcome message;
 // b) Insanity checks;
 // c) Book and nest all lists;
 // d) Book all profiles holding flags;
 // e) Common labels and intervals;
 // f) Book all control histograms for event distributions;
 // g) Book all control histograms for particle distributions;
 // h) Book all histograms for particle weights;
 // i) Book all histograms for multiparticle correlations (and nested loops);
 // j) Book all objects for Toy Monte Carlo ('flow analysis on-the-fly').

 // a) Welcome message:
 cout<<"\n\n:: FlowWithMultiparticleCorrelations::BookEverything()"<<endl;

 // b) Insanity checks:
 //  b0) If the ASCII file holding the input data doesn't exist, bail out:
 if(!fCalculateOnTheFly)
 {
  //for(Int_t id=0;id<2;id++) // TBI_20200620 temporary and ugly workaround
  for(Int_t id=0;id<1;id++)   // TBI_20200620 temporary and ugly workaround
  {
   if(gSystem->AccessPathName(fInputData[id].Data(),kFileExists))
   {
    cout<<"FATAL: the ASCII file holding the list of files to process doesn't exist !!!!"<<endl;
    cout<<Form("       fInputData[%d] = %s",id,fInputData[id].Data())<<endl;
    exit(0);
   }
  } // for(Int_t id=0;id++;id<2)
 }

 // c) Book and nest all lists:
 this->BookAndNestAllLists();

 // d) Book all profiles holding flags: 
 this->BookAllProfilesHoldingFlags();

 // e) Common labels and intervals:
 TString sBA[2] = {"before cuts","after cuts"};
 Int_t colorsBA[2] = {kRed,kBlue};
 TString sXYZ[3] = {"x","y","z"};
 TString sType[3] = {"reconstructed","reconstructed + MC PID","simulated"};
 TString sTypeV[2] = {"reconstructed","simulated"};
 TString sVariable[5] = {"#varphi","p_{t}","#eta","p","rapidity"}; // variable: 0 = phi, 1 = pt, 2 = eta, 3 = p, 4 = rapidity
 Int_t variableNBins[5] = {360,100000,4000,100000,10000};
 Float_t variableMinMax[5][2] = { {-TMath::Pi(),TMath::Pi()}, // phi
                                  {0.,1000.}, // pt
                                  {-10.,30.}, // eta
                                  {0.,1000.}, // p
                                  {-10.,30.} // rapidity                                            
                                };
 TString spVariable[1] = {"PDG code"}; // PID variable: 0 = PDG code
 Int_t pvariableNBins[1] = {200000};
 Float_t pvariableMinMax[1][2] = { {-100000,100000} // PDG code
                                 };

 // f) Book all control histograms for event distributions:
 for(Int_t ba=0;ba<2;ba++)
 {
  // fill or not:
  if(!fFillControlHistogramsEvents[ba]) continue;

  // multiplicity: 
  fMultiplicity[ba] = new TH1F(Form("fMultiplicity[%d]",ba),"",1000,0.,1000.); // TBI_20200513 hardwired limits
  //fMultiplicity[ba]->SetStats(kFALSE);
  fMultiplicity[ba]->SetTitle(sBA[ba].Data());
  fMultiplicity[ba]->GetXaxis()->SetTitle("multiplicity");
  fMultiplicity[ba]->SetFillColor(colorsBA[ba]-10);
  fMultiplicity[ba]->SetLineColor(colorsBA[ba]);
  fControlHistogramsEventsList->Add(fMultiplicity[ba]);

  // orientation of impact parameter vector:
  for(Int_t rs=0;rs<2;rs++) // [reco,sim]
  {  
   fImpactParameter[ba][rs] = new TH1F(Form("fImpactParameter[%d][%d]",ba,rs),"",360,0.,TMath::TwoPi());
   //fImpactParameter[ba][rs]->SetStats(kFALSE);
   fImpactParameter[ba][rs]->SetTitle(Form("%s, %s",sBA[ba].Data(),sTypeV[rs].Data()));
   fImpactParameter[ba][rs]->GetXaxis()->SetTitle("orientation of impact parameter");
   fImpactParameter[ba][rs]->SetFillColor(colorsBA[ba]-10);
   fImpactParameter[ba][rs]->SetLineColor(colorsBA[ba]);
   fControlHistogramsEventsList->Add(fImpactParameter[ba][rs]);
 
   for(Int_t xyz=0;xyz<3;xyz++)
   {
    // vertex:
    fVertex[ba][rs][xyz] = new TH1F(Form("fVertex[%d][%d][%d]",ba,rs,xyz),"",10000,-10.,10.); // TBI_20200513 hardwired limits
    //fVertex[ba][rs][xyz]->SetStats(kFALSE);
    fVertex[ba][rs][xyz]->SetTitle(Form("%s, %s, %s",sBA[ba].Data(),sXYZ[xyz].Data(),sTypeV[rs].Data()));
    fVertex[ba][rs][xyz]->GetXaxis()->SetTitle(Form("V_{%s}",sXYZ[xyz].Data()));
    fVertex[ba][rs][xyz]->SetFillColor(colorsBA[ba]-10);
    fVertex[ba][rs][xyz]->SetLineColor(colorsBA[ba]);
    fControlHistogramsEventsList->Add(fVertex[ba][rs][xyz]); 
   } 
  } // for(Int_t xyz=0;xyz<3;xyz++)
 } // for(Int_t ba=0;ba<2;ba++)

 // g) Book all control histograms for particle distributions:
 for(Int_t ba=0;ba<2;ba++)
 {
  // fill or not:
  if(!fFillControlHistogramsParticles[ba]) continue;

  for(Int_t xyz=0;xyz<3;xyz++)
  {
   for(Int_t t=0;t<3;t++)
   { 
    // DCA:
    fDCA[ba][t][xyz] = new TH1F(Form("fDCA[%d][%d][%d]",ba,t,xyz),"",10000,-100.,100.); // TBI_20200513 hardwired limits
    //fDCA[ba][xyz]->SetStats(kFALSE);
    fDCA[ba][t][xyz]->SetTitle(Form("%s, %s, %s",sType[t].Data(),sXYZ[xyz].Data(),sBA[ba].Data()));
    fDCA[ba][t][xyz]->GetXaxis()->SetTitle(Form("DCA_{%s}",sXYZ[xyz].Data()));
    fDCA[ba][t][xyz]->SetFillColor(colorsBA[ba]-10);
    fDCA[ba][t][xyz]->SetLineColor(colorsBA[ba]);
    fControlHistogramsParticlesList->Add(fDCA[ba][t][xyz]); 
   } // for(Int_t t=0;t<3;t++) 
  } // for(Int_t xyz=0;xyz<3;xyz++)

  // kinematics:
  for(Int_t t=0;t<3;t++)
  { 
   for(Int_t v=0;v<5;v++) // variable: 0 = phi, 1 = pt, 2 = eta, 3 = p, 4 = rapidity
   { 
    fKinematics[ba][t][v] = new TH1F(Form("fKinematics[%d][%d][%d]",ba,t,v),"",variableNBins[v],variableMinMax[v][0],variableMinMax[v][1]); // TBI_20200515 hw limits
    //fKinematics[ba][t][v]->SetStats(kFALSE);
    fKinematics[ba][t][v]->SetTitle(Form("%s, %s, %s",sType[t].Data(),sVariable[v].Data(),sBA[ba].Data()));
    fKinematics[ba][t][v]->GetXaxis()->SetTitle(sVariable[v].Data());
    fKinematics[ba][t][v]->SetFillColor(colorsBA[ba]-10);
    fKinematics[ba][t][v]->SetLineColor(colorsBA[ba]);
    fControlHistogramsParticlesList->Add(fKinematics[ba][t][v]);
   } // for(Int_t v=0;v<4;v++)
  } // for(Int_t t=0;t<3;t++)

  // PID:
  for(Int_t t=0;t<3;t++)
  { 
   for(Int_t pv=0;pv<1;pv++) // PID variable: 0 = PDG code
   { 
    fPID[ba][t][pv] = new TH1F(Form("fPID[%d][%d][%d]",ba,t,pv),"",pvariableNBins[pv],pvariableMinMax[pv][0],pvariableMinMax[pv][1]);
    //fPID[ba][t][pv]->SetStats(kFALSE);
    fPID[ba][t][pv]->SetTitle(Form("%s, %s, %s",sType[t].Data(),spVariable[pv].Data(),sBA[ba].Data()));
    fPID[ba][t][pv]->GetXaxis()->SetTitle(spVariable[pv].Data());
    fPID[ba][t][pv]->SetFillColor(colorsBA[ba]-10);
    fPID[ba][t][pv]->SetLineColor(colorsBA[ba]);
    fControlHistogramsParticlesList->Add(fPID[ba][t][pv]);
   } // for(Int_t v=0;v<4;v++)
  } // for(Int_t t=0;t<3;t++) 

 } // end of for(Int_t ba=0;ba<2;ba++)

 // h) Book all histograms for particle weights:
 Int_t colorsW[4] = {kRed,kBlue,kGreen+2,kBlack};
 TString swVariable[4] = {"#varphi","p_{t}","#eta","rapidity"}; // [phi,pt,eta,rapidity]
 Int_t wvariableNBins[4] = {360,100000,4000,10000};
 Float_t wvariableMinMax[4][2] = { {-TMath::Pi(),TMath::Pi()}, // phi
                                  {0.,1000.}, // pt
                                  {-10.,30.}, // eta
                                  {-10.,30.} // rapidity                                            
                                };

 for(Int_t v=0;v<4;v++) // use weights [phi,pt,eta,rapidity]
 {
  if(!fUseWeights[v]){continue;}
  if(!fWeightsHist[v]) // yes, because these histos are cloned from the exteral ones, see SetWeightsHist(TH1F* const hist, const char *variable)
  {
   fWeightsHist[v] = new TH1F(Form("fWeightsHist[%d]",v),"",wvariableNBins[v],wvariableMinMax[v][0],wvariableMinMax[v][1]);
   fWeightsHist[v]->SetTitle(Form("Particle weights for %s",swVariable[v].Data()));
   fWeightsHist[v]->SetStats(kFALSE);
   fWeightsHist[v]->GetXaxis()->SetTitle(swVariable[v].Data());
   fWeightsHist[v]->SetFillColor(colorsW[v]-10);
   fWeightsHist[v]->SetLineColor(colorsW[v]);
  }
  fWeightsList->Add(fWeightsHist[v]);
 } // for(Int_t v=0;v<4;v++) // use weights [phi,pt,eta,rapidity]

 // i) Book all histograms for multiparticle correlations (and nested loops):
 if(fCalculateCorrelations || fCalculateNestedLoops)
 {
  TString csVariable[2] = {"cos","sin"};
  TString oVariable[4] = {"#varphi_{1}-#varphi_{2}","#varphi_{1}+#varphi_{2}-#varphi_{3}-#varphi_{4}",
                          "#varphi_{1}+#varphi_{2}+#varphi_{3}-#varphi_{4}-#varphi_{5}-#varphi_{6}",
                          "#varphi_{1}+#varphi_{2}+#varphi_{3}+#varphi_{4}-#varphi_{5}-#varphi_{6}-#varphi_{7}-#varphi_{8}"};
  Int_t vvvariableNBins[3] = {1,10000,100}; // TBI_20200612 add setters
  Float_t vvvariableMinMax[3][2] = { {0.,1.}, // integrated 
                                     {0.,10000.}, // multiplicity
                                     {0.,100.} // centrality
                                   };
  TString vvVariable[3] = {"integrated","multiplicity","centrality"};
  for(Int_t cs=0;cs<2;cs++) // cos(...) or sin (...) [0=cos,1=sin]
  {
   for(Int_t k=0;k<4;k++) // order [2p=0,4p=1,6p=2,8p=3]
   {  
    for(Int_t n=0;n<6;n++) // harmonic [n=1,n=2,...,n=6]
    {
     for(Int_t v=0;v<3;v++) // variable [0=integrated,1=vs. multiplicity,2=vs. centrality]
     {
      if(fCalculateCorrelations)
      {
       fCorrelationsPro[cs][k][n][v] = new TProfile(Form("fCorrelationsPro[%d][%d][%d][%d]",cs,k,n,v),Form("#LT#LT%s[%s(%s)]#GT#GT",csVariable[cs].Data(),1==n+1?"":Form("%d",n+1),oVariable[k].Data()),vvvariableNBins[v],vvvariableMinMax[v][0],vvvariableMinMax[v][1]);
       fCorrelationsPro[cs][k][n][v]->SetStats(kFALSE);
       fCorrelationsPro[cs][k][n][v]->Sumw2();
       fCorrelationsPro[cs][k][n][v]->GetXaxis()->SetTitle(vvVariable[v].Data());
       //fCorrelationsPro[cs][k][n][v]->SetFillColor(colorsW[v]-10);
       //fCorrelationsPro[cs][k][n][v]->SetLineColor(colorsW[v]);
       fCorrelationsList->Add(fCorrelationsPro[cs][k][n][v]);
      } // if(fCalculateCorrelations)
      if(fCalculateNestedLoops)
      {
       fNestedLoopsPro[cs][k][n][v] = new TProfile(Form("fNestedLoopsPro[%d][%d][%d][%d]",cs,k,n,v),Form("#LT#LT%s[%s(%s)]#GT#GT",csVariable[cs].Data(),1==n+1?"":Form("%d",n+1),oVariable[k].Data()),vvvariableNBins[v],vvvariableMinMax[v][0],vvvariableMinMax[v][1]);
       fNestedLoopsPro[cs][k][n][v]->SetStats(kFALSE);
       fNestedLoopsPro[cs][k][n][v]->Sumw2();
       fNestedLoopsPro[cs][k][n][v]->GetXaxis()->SetTitle(vvVariable[v].Data());
       //fNestedLoopsPro[cs][k][n][v]->SetFillColor(colorsW[v]-10);
       //fNestedLoopsPro[cs][k][n][v]->SetLineColor(colorsW[v]);
       fNestedLoopsList->Add(fNestedLoopsPro[cs][k][n][v]);  
      } // if(fCalculateNestedLoops)   
     } // for(Int_t v=0;v<2;v++) // variable [0=integrated,1=vs. multiplicity,2=vs. centrality]
    } // for(Int_t n=0;n<6;n++) // harmonic [n=1,n=2,...,n=6]
   } // for(Int_t n=0;n<6;n++) // harmonics [n=1,n=2,...,n=6]
  } // for(Int_t cs=0;cs<2;cs++) // cos(...) or sin (...) [0=cos,1=sin]
 } // if(fCalculateCorrelations || fCalculateNestedLoops) 

 if(fCalculateNestedLoops)
 { 
  Int_t maxSize = 10000; // TBI_20200613 this is also needed in void CalculateNestedLoops(void) => shall I promote it to data member?
  ftaNestedLoops[0] = new TArrayF(maxSize); // container for azimuthal angles 
  ftaNestedLoops[1] = new TArrayF(maxSize); // container for particle angles  
 }

 if(fCalculateOnTheFly)
 {
  this->BookAllOnTheFlyObjects();
 }

} // void FlowWithMultiparticleCorrelations::BookEverything() fInputData

//=======================================================================================

void FlowWithMultiparticleCorrelations::ProcessEvents() 
{
 // For each event only this method is executed. 

 // a) Welcome message; 
 // b) Toy Monte Carlo (flow analysis 'on-the-fly');
 // c) Book VK objects;
 // d) Prepare the dataset;
 //  d0) Book and fill the local chain; 
 //  d1) Get the "DataHeader" and "Configuration";
 //  d2) DCA thingie;
 // e) Main loop over events;  
 //  e0) Fill multiplicity before cuts;
 //  e1) Fill vertex before cuts;
 //  e2) Fill centrality after cuts; 
 //  e3) Fill orientation of impact parameter vector before cuts;
 // f) Apply event cuts;
 // g) Main loop over the tracks;
 //  g0) Track cuts;
 // h) Fill event quantities after all cuts;
 //  h0) Fill multiplicity after cuts;
 //  h1) Fill vertex after cuts;
 //  h2) Fill centrality after cuts; 
 //  h3) Fill orientation of impact parameter vector after cuts;
 // ...) Delete all 'new' pointers.

 // a) Welcome message:
 cout<<"\n\n:: FlowWithMultiparticleCorrelations::ProcessEvents()"<<endl;

 // b) Toy Monte Carlo (flow analysis 'on-the-fly'):
 if(fCalculateOnTheFly)
 {
  for(Int_t e=0;e<fnEvents;e++)
  {
   cout<<Form("%.2f%%\r",100.*e/fnEvents); 
   this->CalculateOnTheFly();
  } 
  return; 
 }

 // c) Book VK objects:
 EventHeader* sim_event_header {new EventHeader};
 EventHeader* rec_event_header {new EventHeader};
 Container* ana_event_header {new Container}; // 20200504 doesn't work TBI
 Particles* sim_particles {new Particles};
 Particles* rec_particles {new Particles}; // 20200504 doesn't work TBI
 TrackDetector* rec_tracks {new TrackDetector};
 Matching* rec2sim_match {new Matching};

 // d) Prepare the dataset:
 string line;
 ifstream myfile;
 myfile.open(fInputData[0].Data());
 Int_t nFiles=0;
 while ( getline (myfile,line) ) // TBI just a temporary solution to count number of files, re-implement eventually  
 { 
  nFiles++;
 } // while ( getline (myfile,line) )
 myfile.close(); // TBI clearly this is an overkill, but is neeed as it seems
 cout<<Form("=> There %s %d file%s to process .... ",1==nFiles?"is":"are",nFiles,1==nFiles?"":"s")<<endl;

 //  d0) Book and fill the local chain:
 Int_t nFilesMessage = 0; -1 == fMaxNoFiles ? nFilesMessage = nFiles : nFilesMessage = fMaxNoFiles; // TBI not sure if this is correct
 cout<<Form("=> Adding %d file%s in the \"reco\" chain .... ",nFilesMessage,1==nFilesMessage?"":"s")<<endl;
 TChain *chainReco = new TChain("aTree");
 chainReco->SetBranchAddress("SimEventHeader", &sim_event_header);
 chainReco->SetBranchAddress("RecEventHeader", &rec_event_header);
 chainReco->SetBranchAddress("VtxTracks", &rec_tracks);
 chainReco->SetBranchAddress("SimTracks", &sim_particles);
 chainReco->SetBranchAddress("VtxTracks2SimTracks", &rec2sim_match);
 myfile.open(fInputData[0].Data()); // TBI clearly this is an overkill, but is neeed as it seems, otherwise it doesn't parse from scratch
 Int_t fileCounter=0;
 while ( getline (myfile,line) )
 {
  if(fileCounter++ == fMaxNoFiles){break;}
  chainReco->Add(line.data());  
 }
 cout<<Form("   Done! .... ")<<endl;
 myfile.close(); // TBI clearly this is an overkill, but is neeed as it seems

 /* TBI_20200523 waiting for further feedback from VK
 TChain *chainAnalysis = new TChain("cTree");
 chainAnalysis->SetBranchAddress("AnaEventHeader", &ana_event_header);
 chainAnalysis->SetBranchAddress("RecParticlesMcPid", &rec_particles);
 cout<<"=> Adding a files to \"analysis\" chain .... "<<endl;
 myfile.open(fInputData[1].Data()); // TBI clearly this is an overkill, but is neeed as it seems, otherwise it doesn't parse from scratch
 while ( getline (myfile,line) )
 {
  chainAnalysis->Add(line.data());  
 }
 cout<<Form("   Done! .... ")<<endl;
 myfile.close(); // TBI clearly this is an overkill, but is neeed as it seems
 */

 //  d1) Get the "DataHeader" and "Configuration":
 // TBI_20200519: 
 //      a) data header is the same in all files, check online documentation what this actually is
 //      b) I have below just hardwired one example file, to get "DataHeader", eventually:
 //         b0) randomize all files and take one file randomly. Shall I still check that DataHeader printout is always the same?   
 //      c) Same for "Configuration"
 cout<<Form("=> Getting the \"DataHeader\" and \"Configuration\" .... ")<<endl;
 TFile *fileReco = TFile::Open("/home/abilandz/data/cbm/urqmd_eos0_auau_12agev_mbias_psd44_hole20_pipe0_TGeant3/analysis_tree/1000.analysistree.root","read"); // TBI_20200523 temporary hardwired 
 DataHeader* data_header = (DataHeader*) fileReco->Get("DataHeader");
 // data_header->Print();
 Configuration* configReco = (Configuration*) fileReco->Get("Configuration");
 //configReco->Print();
 //TFile *fileAnalysis = TFile::Open("/home/abilandz/data/cbm/urqmd_eos0_auau_12agev_mbias_psd44_hole20_pipe0_TGeant3/filler/000.analysistree.root","read"); // TBI_20200523 temporary hardwired 
 //Configuration* configAnalysis = (Configuration*) fileAnalysis->Get("Configuration");
 //configAnalysis->Print();
 cout<<Form("   Done! .... ")<<endl;

 //  d2) DCA thingie:
 // VK: DCA is strictly speaking not DCA, and is defined as:
 // o for "primaries" (close to primary vertex): the distance between track extrapolation to Z coordinate of the vertex and primary vertex position - that is why DCA_Z in most cases exactly 0.
 // o for "secondaries": the distance between first hit and PV. DCA_Z in this case - position of the first station of this track.
 const int dcax_id = configReco->GetBranchConfig("VtxTracks").GetFieldId("dcax");
 const int dcay_id = configReco->GetBranchConfig("VtxTracks").GetFieldId("dcay");
 const int dcaz_id = configReco->GetBranchConfig("VtxTracks").GetFieldId("dcaz");

 // e) Main loop over events:
 cout<<Form("=> Starting the main loop over events .... ")<<endl;
 Int_t nEvents = chainReco->GetEntries();
 cout << "   Total number of events: " << nEvents << endl; // Yes, TChain is smart, it sums up all events from all TTrees added to it
 cout << endl;
 for(Int_t event=0; event<nEvents; event++)
 {
  // Reset all event-by-event quantities:
  this->ResetEventByEventQuantities();

  chainReco->GetEntry(event);
  const int nTracks = rec_tracks->GetNumberOfChannels();
  if(0==event%100){cout<<Form("   %.2f%% : Multiplicity = %d\r",100.*event/nEvents,nTracks)<<endl;}

  //  e0) Fill multiplicity before cuts:
  if(fMultiplicity[0]){fMultiplicity[0]->Fill(nTracks);}
  //  e1) Fill vertex before cuts:
  //   Reconstructed:
  const TVector3 rec_vtx_pos = rec_event_header->GetVertexPosition3();
  if(fVertex[0][0][0]){fVertex[0][0][0]->Fill(rec_vtx_pos.X());}
  if(fVertex[0][0][1]){fVertex[0][0][1]->Fill(rec_vtx_pos.Y());}
  if(fVertex[0][0][2]){fVertex[0][0][2]->Fill(rec_vtx_pos.Z());}
  //   Simulated:
  const TVector3 sim_vtx_pos = sim_event_header->GetVertexPosition3(); // TBI focusing only on re. for the time being
  if(fVertex[0][1][0]){fVertex[0][1][0]->Fill(sim_vtx_pos.X());}
  if(fVertex[0][1][1]){fVertex[0][1][1]->Fill(sim_vtx_pos.Y());}
  if(fVertex[0][1][2]){fVertex[0][1][2]->Fill(sim_vtx_pos.Z());}
  //  e2) Fill centrality before cuts: 
  //   const auto centrality = ana_event_header->GetField<float>(0);  //NOTE hardcoded 0, to be fixed TBI doesn't work
  //  e3) Fill orientation of impact parameter vector before cuts:
  //   Reconstructed:
  if(fImpactParameter[0][0]){fImpactParameter[0][0]->Fill(rec_event_header->GetField<float>(0));}
  //   Simulated:
  if(fImpactParameter[0][1]){fImpactParameter[0][1]->Fill(sim_event_header->GetField<float>(0));}

  // f) Apply event cuts:
  if(nTracks < fMultiplicityCuts[0] || nTracks > fMultiplicityCuts[1]){continue;}
  if(rec_vtx_pos.X() < fVertexCuts[0][0] || rec_vtx_pos.X() > fVertexCuts[0][1]){continue;}
  if(rec_vtx_pos.Y() < fVertexCuts[1][0] || rec_vtx_pos.Y() > fVertexCuts[1][1]){continue;}
  if(rec_vtx_pos.Z() < fVertexCuts[2][0] || rec_vtx_pos.Z() > fVertexCuts[2][1]){continue;}
  //  centrality cuts

  // g) Main loop over the tracks:
  for(int i_track=0; i_track<nTracks; ++i_track)
  {
   // reconstructed track:
   const auto& rec_track = rec_tracks->GetChannel(i_track);
   // DCA before the cuts: 
   if(fDCA[0][0][0]){fDCA[0][0][0]->Fill(rec_track.GetField<float>(dcax_id));}
   if(fDCA[0][0][1]){fDCA[0][0][1]->Fill(rec_track.GetField<float>(dcay_id));}
   if(fDCA[0][0][2]){fDCA[0][0][2]->Fill(rec_track.GetField<float>(dcaz_id));}
   // kinematics before the cuts:
   if(fKinematics[0][0][0]){fKinematics[0][0][0]->Fill(rec_track.GetPhi());}
   if(fKinematics[0][0][1]){fKinematics[0][0][1]->Fill(rec_track.GetPt());}
   if(fKinematics[0][0][2]){fKinematics[0][0][2]->Fill(rec_track.GetEta());}
   if(fKinematics[0][0][3]){fKinematics[0][0][3]->Fill(rec_track.GetP());}
   // TBI_20200606 fill also if(fKinematics[0][0][4]) for rapidity, not directly available at the moment
  
   // reconstructed track with MC PID (TBI check with VK and IS if this is correct)
   //const auto& rec_particle = rec_particles->GetChannel(i_track); // TBI 20200506 doesn't work yes, this is related to "RecParticlesMcPid"
   //std::cout << "            phi = " << rec_particle.GetPhi() << std::endl;
   //std::cout << "            pid = " << rec_particle.GetPid() << std::endl;


   // simulated particles before the cuts:
   const int sim_id = rec2sim_match->GetMatch(i_track);
   if(sim_id>=0)
   {
    const auto& sim_track = sim_particles->GetChannel(sim_id);
    //  kinematics:
    if(fKinematics[0][2][0]){fKinematics[0][2][0]->Fill(sim_track.GetPhi());}
    if(fKinematics[0][2][1]){fKinematics[0][2][1]->Fill(sim_track.GetPt());}
    if(fKinematics[0][2][2]){fKinematics[0][2][2]->Fill(sim_track.GetEta());}
    if(fKinematics[0][2][3]){fKinematics[0][2][3]->Fill(sim_track.GetP());}
    if(fKinematics[0][2][4]){fKinematics[0][2][4]->Fill(sim_track.GetRapidity());}
    //  PID:
    if(fPID[0][2][0]){fPID[0][2][0]->Fill(sim_track.GetPid());}
   } // if(sim_id>=0)



   // TBI_20200523 apply some track cuts here
   if(rec_track.GetPt()>5.){continue;} // add setter for this
   // ...



   // Reconstructed particles after the cuts:
   // DCA after the cuts: 
   if(fDCA[1][0][0]){fDCA[1][0][0]->Fill(rec_track.GetField<float>(dcax_id));}
   if(fDCA[1][0][1]){fDCA[1][0][1]->Fill(rec_track.GetField<float>(dcay_id));}
   if(fDCA[1][0][2]){fDCA[1][0][2]->Fill(rec_track.GetField<float>(dcaz_id));}
   // kinematics after the cuts:
   if(fKinematics[1][0][0]){fKinematics[1][0][0]->Fill(rec_track.GetPhi());}
   if(fKinematics[1][0][1]){fKinematics[1][0][1]->Fill(rec_track.GetPt());}
   if(fKinematics[1][0][2]){fKinematics[1][0][2]->Fill(rec_track.GetEta());}
   if(fKinematics[1][0][3]){fKinematics[1][0][3]->Fill(rec_track.GetP());}

   // Simulated particles after the cuts:
   if(sim_id>=0)
   {
    const auto& sim_track = sim_particles->GetChannel(sim_id); // TBI_20200606 there is some duplication here, see the same code above for 'before cuts'
    //  kinematics:
    if(fKinematics[1][2][0]){fKinematics[1][2][0]->Fill(sim_track.GetPhi());}
    if(fKinematics[1][2][1]){fKinematics[1][2][1]->Fill(sim_track.GetPt());}
    if(fKinematics[1][2][2]){fKinematics[1][2][2]->Fill(sim_track.GetEta());}
    if(fKinematics[1][2][3]){fKinematics[1][2][3]->Fill(sim_track.GetP());}
    if(fKinematics[1][2][4]){fKinematics[1][2][4]->Fill(sim_track.GetRapidity());}
    //  PID:
    if(fPID[1][2][0]){fPID[1][2][0]->Fill(sim_track.GetPid());}
   } // if(sim_id>=0)

   // Fill Q-vector components:
   if(fCalculateQvector || fCalculateNestedLoops)
   {
    // Declare:
    Float_t dPhi = 0., wPhi = 1.; // azimuthal angle and corresponding phi weight
    Float_t dPt = 0., wPt = 1.; // transverse momentum and corresponding pT weight
    Float_t dEta = 0., wEta = 1.; // pseudorapidity and corresponding eta weight
    //Float_t dRapidity = 0., wRapidty = 1.; // rapidity and corresponding rapidity weight TBI_20200612 enable eventually
    Float_t wToPowerP = 1.; // weight raised to power p
    // Access:
    dPhi = rec_track.GetPhi();
    if(isnan(dPhi)){continue;} // TBI_20200614 just a temporary workaround
    if(fUseWeights[0]){wPhi = Weight(dPhi,"phi");} // corresponding phi weight
    dPt = rec_track.GetPt();
    if(isnan(dPt)){continue;} // TBI_20200614 just a temporary workaround
    if(fUseWeights[1]){wPt = Weight(dPt,"pt");} // corresponding pt weight
    dEta = rec_track.GetEta();
    if(isnan(dEta)){continue;} // TBI_20200614 just a temporary workaround
    if(fUseWeights[2]){wEta = Weight(dEta,"eta");} // corresponding eta weight
    //dRapidity = rec_track.GetRapidty(); TBI_20200612 enable eventually
    //if(isnan(dRapidity)){continue;} // TBI_20200614 just a temporary workaround
    //if(fUseWeights[3]){wRapidty = Weight(dRapidity,"rapidity");} // corresponding rapidity weight TBI_20200612 enable eventually
    // Fill containers for nested loops:
    if(ftaNestedLoops[0]){ftaNestedLoops[0]->AddAt(dPhi,fnSelectedTracksEBE);} 
    if(ftaNestedLoops[1]){ftaNestedLoops[1]->AddAt(wPhi*wPt*wEta,fnSelectedTracksEBE);} 
    // Finally, calculate Q-vectors:
    for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++)
    {
     for(Int_t wp=0;wp<fMaxCorrelator+1;wp++) // weight power
     {
     if(fUseWeights[0]||fUseWeights[1]||fUseWeights[2]||fUseWeights[3]){wToPowerP = pow(wPhi*wPt*wEta,wp);} 
     //if(fUseWeights[0]||fUseWeights[1]||fUseWeights[2]||fUseWeights[3]){wToPowerP = pow(wPhi*wPt*wEta*wRapidity,wp);} // TBI_20200612 same as above, just taking also wRapidity
      fQvector[h][wp] += TComplex(wToPowerP*TMath::Cos(h*dPhi),wToPowerP*TMath::Sin(h*dPhi));
     } // for(Int_t wp=0;wp<fMaxCorrelator+1;wp++)
    } // for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++)   
   } // if(fCalculateQvector || fCalculateNestedLoops)

   // Track is selected for analysis: 
   fnSelectedTracksEBE++;

  } // for(int i_track=0; i_track<nTracks; ++i_track)

  if(0==event%100){cout<<Form("   %.2f%% : Selected tracks = %d\r",100.*event/nEvents,fnSelectedTracksEBE)<<endl;}

  // Calculate multiparticle correlations:
  if(fCalculateQvector && fCalculateCorrelations)
  {
   this->CalculateCorrelations();
  }

  // Calculate nested loops:
  if(fCalculateNestedLoops)
  {
   this->CalculateNestedLoops();
  }

  // h) Fill event quantities after all cuts;
  //  h0) Fill multiplicity after all event and track cuts:
  if(fMultiplicity[1]){fMultiplicity[1]->Fill(fnSelectedTracksEBE);}
  //  h1) Fill vertex after cuts:
  //   Reconstructed:
  if(fVertex[1][0][0]){fVertex[1][0][0]->Fill(rec_vtx_pos.X());}
  if(fVertex[1][0][1]){fVertex[1][0][1]->Fill(rec_vtx_pos.Y());}
  if(fVertex[1][0][2]){fVertex[1][0][2]->Fill(rec_vtx_pos.Z());}
  //   Simulated:
  if(fVertex[1][1][0]){fVertex[1][1][0]->Fill(sim_vtx_pos.X());}
  if(fVertex[1][1][1]){fVertex[1][1][1]->Fill(sim_vtx_pos.Y());}
  if(fVertex[1][1][2]){fVertex[1][1][2]->Fill(sim_vtx_pos.Z());}

  //  h2) Fill centrality after cuts: 
  // ...

  //  h3) Fill orientation of impact parameter vector after cuts:
  //   Reconstructed:
  if(fImpactParameter[1][0]){fImpactParameter[1][0]->Fill(rec_event_header->GetField<float>(0));}
  //   Simulated:
  if(fImpactParameter[1][1]){fImpactParameter[1][1]->Fill(sim_event_header->GetField<float>(0));}   

 } // for(long event=0; event<nEvents; event++)

 // ...) Delete all 'new' pointers:
 delete chainReco;
 // VK objects:
 delete sim_event_header;
 delete rec_event_header;
 //delete ana_event_header; // 20200504 doesn't work TBI
 delete sim_particles;
 //delete rec_particles; // 20200504 doesn't work TBI
 delete rec_tracks;
 delete rec2sim_match;

} // void FlowWithMultiparticleCorrelations::ProcessEvents() 

//=======================================================================================

void FlowWithMultiparticleCorrelations::Terminate() 
{
 // Called only after all events are processed.

 // a) Welcome message;
 // b) Get the pointer to the base list holding all objects;
 // c) Final touch for the cross-check with the nested loops;
 // d) Dump the base list with all objects in the final ouput file.

 // a) Welcome message; 
 cout<<"\n\n:: FlowWithMultiparticleCorrelations::Terminate()"<<endl; 

 // b) Get the pointer to the base list holding all objects: TBI 

 // c) Final touch for the cross-check with the nested loops:
 cout<<"=> The comparison between the nested loops and correlations: .... "<<endl;
 if(fCalculateNestedLoops){this->ComparisonNestedLoopsVsCorrelations();}
 cout<<"   Done! .... "<<endl;

 // d) Dump the base list with all objects in the final ouput file.
 // TBI_20200513 protection against empty file 
 cout<<Form("=> Saving all results in the file \"%s\" .... ",fOutputFileName.Data())<<endl;
 TFile *file = new TFile(fOutputFileName.Data(),"RECREATE"); 
 fHistList->Write(fHistList->GetName(),TObject::kSingleKey);
 cout<<"   Done! .... "<<endl;
 delete file;

 cout<<"\n\n   Hasta la vista! .... "<<endl;

} // void FlowWithMultiparticleCorrelations::Terminate() 

//=======================================================================================

void FlowWithMultiparticleCorrelations::BookAndNestAllLists()
{
 // Book and nest all lists within base list fHistlist.

 // a) Book and nest lists for control histograms;
 // b) Book and nest lists for particle weights;
 // c) Book and nest lists for Q-vectors;
 // d) Book and nest all lists for nested loops;
 // e) Book and nest all lists for nested loops;
 // f) Book and nest all lists for Toy Monte Carlo (flow analysis 'on-the-fly').

 TString sMethodName = "void FlowWithMultiparticleCorrelations::BookAndNestAllLists()";
 if(!fHistList){cout<<Form("%s fHistList is NULL",sMethodName.Data())<<endl; exit(0);}

 // a) Book and nest lists for control histograms:
 fControlHistogramsList = new TList();
 fControlHistogramsList->SetName("Control histograms");
 fControlHistogramsList->SetOwner(kTRUE);
 fHistList->Add(fControlHistogramsList);
 // Global event observables:
 fControlHistogramsEventsList = new TList();
 fControlHistogramsEventsList->SetName("Event observables");
 fControlHistogramsEventsList->SetOwner(kTRUE);
 fControlHistogramsList->Add(fControlHistogramsEventsList);
 // Particle distributions:
 fControlHistogramsParticlesList = new TList();
 fControlHistogramsParticlesList->SetName("Particle distributions");
 fControlHistogramsParticlesList->SetOwner(kTRUE);
 fControlHistogramsList->Add(fControlHistogramsParticlesList);

 // b) Book and nest lists for particle weights:
 fWeightsList = new TList();
 fWeightsList->SetName("Particle weights");
 fWeightsList->SetOwner(kTRUE);
 fHistList->Add(fWeightsList);

 // c) Book and nest lists for Q-vectors:
 fQvectorList = new TList();
 fQvectorList->SetName("Q-vectors");
 fQvectorList->SetOwner(kTRUE);
 fHistList->Add(fQvectorList);

 // d) Book and nest all lists for multiparticle correlations:
 fCorrelationsList = new TList();
 fCorrelationsList->SetName("Multiparticle correlations");
 fCorrelationsList->SetOwner(kTRUE);
 fHistList->Add(fCorrelationsList);

 // e) Book and nest all lists for nested loops:
 fNestedLoopsList = new TList();
 fNestedLoopsList->SetName("Nested loops");
 fNestedLoopsList->SetOwner(kTRUE);
 fHistList->Add(fNestedLoopsList);

 // f) Book and nest all lists for Toy Monte Carlo (flow analysis 'on-the-fly'):
 fOnTheFlyList = new TList();
 fOnTheFlyList->SetName("On-the-fly");
 fOnTheFlyList->SetOwner(kTRUE);
 fHistList->Add(fOnTheFlyList);

} // void FlowWithMultiparticleCorrelations::BookAndNestAllLists()

//=======================================================================================

void FlowWithMultiparticleCorrelations::InitializeArrays()
{
 // Initialize all arrays to null. It's done here, because this cannot be done in the constructor list.

 // a) Input data;
 // b) Initialize all arrays for events;
 // c) Initialize all arrays for particles;
 // d) Initialize all arrays for particle weights;
 // e) Initialize all arrays for Q-vectors;
 // f) Initialize all arrays for multiparticle correlations (and nested loops);
 // g) Initialize arrazs for Toy Monte Carlo (flow analysis 'on-the-fly').

 cout<<":: FlowWithMultiparticleCorrelations::InitializeArrays()"<<endl; 

 // a) Input data:
 fInputData[0] = "";
 fInputData[1] = "";

 // b) Initialize all arrays for events:
 //  Control histograms for events:
 for(Int_t ba=0;ba<2;ba++) // [before,after] cuts
 {
  fFillControlHistogramsEvents[ba] = kTRUE; // fill 'em all by default 
 }
 //  Multiplicity:
 for(Int_t ba=0;ba<2;ba++) // [before,after] cuts
 {
  fMultiplicity[ba] = NULL;
 }

 // Orientation of impact parameter vector:
 for(Int_t ba=0;ba<2;ba++)
 {  
  for(Int_t rs=0;rs<2;rs++)
  {
   fImpactParameter[ba][rs] = NULL;
  }
 }

 //  Vertex:
 for(Int_t ba=0;ba<2;ba++)
 {  
  for(Int_t rs=0;rs<2;rs++)
  {
   for(Int_t xyz=0;xyz<3;xyz++)
   {
    fVertex[ba][rs][xyz] = NULL;
   }
  }
 } 

 // c) Initialize all arrays for particles:
 //  Control histograms for particles:
 for(Int_t ba=0;ba<2;ba++)
 {
  fFillControlHistogramsParticles[ba] = kTRUE; // fill 'em by default 
 }
 //  Kinematics:
 for(Int_t ba=0;ba<2;ba++)
 {
  for(Int_t t=0;t<3;t++) // type
  { 
   for(Int_t v=0;v<5;v++) // variable: 0 = phi, 1 = pt, 2 = eta, 3 = p, 4 = rapidity
   {
    fKinematics[ba][t][v] = NULL;
   } // for(Int_t v=0;v<4;v++)
  } // for(Int_t t=0;t<3;t++)
 } // for(Int_t ba=0;ba<2;ba++)

 //  DCA:
 for(Int_t ba=0;ba<2;ba++)
 {
  for(Int_t xyz=0;xyz<3;xyz++)
  {
   for(Int_t t=0;t<3;t++) // track type: 0 = particles; 1 = tracks TBI_20200515 clarify further
   {
    fDCA[ba][t][xyz] = NULL;
   } // for(Int_t t=0;t<3;t++) 
  } // for(Int_t xyz=0;xyz<3;xyz++)
 } // for(Int_t ba=0;ba<2;ba++)

 // PID:
 for(Int_t ba=0;ba<2;ba++)
 {
  for(Int_t t=0;t<3;t++) // type
  { 
   for(Int_t pv=0;pv<1;pv++) // PID variable: 0 = PDG code
   {
    fPID[ba][t][pv] = NULL;
   } // for(Int_t pv=0;pv<1;pv++) // PID variable: 0 = PDG code
  } // for(Int_t t=0;t<3;t++) // type
 } // for(Int_t ba=0;ba<2;ba++)

 // d) Initialize all arrays for particle weights:
 for(Int_t v=0;v<4;v++) // use weights [phi,pt,eta,rapidity]
 {
  fWeightsHist[v] = NULL;
  fUseWeights[v] = kFALSE;
 } // for(Int_t v=0;v<1;v++) // use weights [phi,pt,eta,rapidity]

 // e) Initialize all arrays for Q-vectors:
 for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++) 
 {
  for(Int_t wp=0;wp<fMaxCorrelator+1;wp++) // weight power
  {
   fQvector[h][wp] = TComplex(0.,0.);
  }
 }

 // f) Initialize all arrays for multiparticle correlations (and nested loops):
 for(Int_t cs=0;cs<2;cs++) // cos(...) or sin (...) [0=cos,1=sin]
 {
  for(Int_t k=0;k<4;k++) // order [2p=0,4p=1,6p=2,8p=3]
  { 
   for(Int_t n=0;n<6;n++) // harmonic [n=1,n=2,...,n=6]
   {
    for(Int_t v=0;v<3;v++) // variable [0=integrated,1=vs. multiplicity,2=vs. centrality]
    {
     fCorrelationsPro[cs][k][n][v] = NULL;
     fNestedLoopsPro[cs][k][n][v] = NULL;
    } // for(Int_t v=0;v<2;v++) // variable [0=integrated,1=vs. multiplicity,2=vs. centrality] 
   } // for(Int_t n=0;n<6;n++) // harmonic [n=1,n=2,...,n=6]
  } // for(Int_t n=0;n<6;n++) // harmonics [n=1,n=2,...,n=6]
 } // for(Int_t cs=0;cs<2;cs++) // cos(...) or sin (...) [0=cos,1=sin]

 for(Int_t aw=0;aw<2;aw++) // [0=angles;1=weights]   
 {
  ftaNestedLoops[aw] = NULL;
 }

 // g) Initialize arrays for Toy Monte Carlo (flow analysis 'on-the-fly'):
 for(Int_t v=0;v<4;v++)
 {
  fNonDefaultPDFs[v] = NULL; 
 }
 fProbability[0] = 1.; // probability that particle is reconstructed in [-3Pi/4,Pi/4]
 fProbability[1] = 1.; // probability that particle is reconstructed in [Pi/3,2Pi/3]
 fMultRangePDF[0] = 8.; 
 fMultRangePDF[1] = 10000;

} // void FlowWithMultiparticleCorrelations::InitializeArrays()

//=======================================================================================

void FlowWithMultiparticleCorrelations::DefaultEventCuts()
{
 // Hardwire here all default cuts.

 // a) Default vertex cuts;
 // b) Multiplicity cuts.

 cout<<":: FlowWithMultiparticleCorrelations::DefaultEventCuts()"<<endl; 

 // a) Default vertex cuts: TBI_20200523 implement more realistic numbers
 fVertexCuts[0][0] = -1000.; // vx min
 fVertexCuts[0][1] = 1000.;  // vx max
 fVertexCuts[1][0] = -1000.; // vy min
 fVertexCuts[1][1] = 1000.;  // vy max
 fVertexCuts[2][0] = -1000.; // vz min
 fVertexCuts[2][1] = 1000.;  // vz max

 // b) Multiplicity cuts:
 fMultiplicityCuts[0] = 0.;
 fMultiplicityCuts[1] = 10000.;

} // void FlowWithMultiparticleCorrelations::DefaultEventCuts()

//=======================================================================================

TH1F *FlowWithMultiparticleCorrelations::GetHistogramWithWeights(const char *filePath, const char *variable)
{
 // Access from external ROOT file the desired histogram with particle weights. 

 // a) Return value; 
 // b) Basic protection for arguments; 
 // c) Check if the external ROOT file exists at specified path; 
 // d) Access the external ROOT file and fetch the desired histogram with weights;
 // e) Close the external ROOT file. 

 // a) Return value:
 TH1F *hist = NULL; 

 // b) Basic protection for arguments:
 if(!(TString(variable).EqualTo("phi") || TString(variable).EqualTo("pt") || TString(variable).EqualTo("eta") || TString(variable).EqualTo("rapidity"))){cout<<__LINE__<<endl;exit(0);} // TBI_20200606 improve the error message

 // c) Check if the external ROOT file exists at specified path:
 if(gSystem->AccessPathName(filePath,kFileExists))
 {
  cout<<Form("if(gSystem->AccessPathName(filePath,kFileExists)), filePath = %s",filePath)<<endl; exit(0);
 }

 // d) Access the external ROOT file and fetch the desired histogram with weights:
 TFile *weightsFile = TFile::Open(filePath,"READ");
 if(!weightsFile) exit(0);
 hist = (TH1F*)(weightsFile->Get(Form("%s",variable)));
 if(!hist) exit(0);
 hist->SetDirectory(0);

 // e) Close the external ROOT file: 
 weightsFile->Close(); delete weightsFile;

 return hist;

} // TH1F *FlowWithMultiparticleCorrelations::GetHistogramWithWeights(const char *filePath, const char *variable)

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::SetWeightsHist(TH1F* const hist, const char *variable)
{
 // Copy histogram holding weights from an external file to the corresponding data member. 
  
 // Basic protection:
 if(!(TString(variable).EqualTo("phi") || TString(variable).EqualTo("pt") || TString(variable).EqualTo("eta") || TString(variable).EqualTo("rapidity"))){cout<<__LINE__<<endl; exit(0);}

 Int_t pper=-1;
 if(TString(variable).EqualTo("phi")){pper=0;} 
 if(TString(variable).EqualTo("pt")){pper=1;} 
 if(TString(variable).EqualTo("eta")){pper=2;} 
 if(TString(variable).EqualTo("rapidity")){pper=3;} 

 // Finally:
 hist->SetDirectory(0);
 fWeightsHist[pper] = (TH1F*)hist->Clone();
 if(!fWeightsHist[pper]){cout<<__LINE__<<endl; exit(0);}

 // Flag:
 fUseWeights[pper] = kTRUE; 

} // void FlowWithMultiparticleCorrelations::SetWeightsHist(TH1D* const hwh, const char *type, const char *variable)

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::BookAllProfilesHoldingFlags()
{
 // Book all permanent profiles holding flags.
 
 // TBI_20200612 add menu

 cout<<":: FlowWithMultiparticleCorrelations::BookAllProfilesHoldingFlags()"<<endl; 

 fControlHistogramsPro = new TProfile("fControlHistogramsPro","flags for control histograms",1,0.,1.);
 fControlHistogramsList->Add(fControlHistogramsPro);
 fControlHistogramsEventsPro = new TProfile("fControlHistogramsEventsPro","flags for event observables",1,0.,1.); 
 fControlHistogramsEventsList->Add(fControlHistogramsEventsPro);
 fControlHistogramsParticlesPro = new TProfile("fControlHistogramsParticlesPro","flags for particle distributions",1,0.,1.);
 fControlHistogramsParticlesList->Add(fControlHistogramsParticlesPro);
 fWeightsPro = new TProfile("fWeightsPro","flags for particle weights",4,0.,4.);
 fWeightsPro->SetStats(kFALSE);
 fWeightsPro->GetXaxis()->SetLabelSize(0.05);  
 fWeightsPro->GetXaxis()->SetBinLabel(1,"w_{#varphi}");  
 fWeightsPro->GetXaxis()->SetBinLabel(2,"w_{p_{t}}");  
 fWeightsPro->GetXaxis()->SetBinLabel(3,"w_{#eta}"); 
 fWeightsPro->GetXaxis()->SetBinLabel(4,"w_{rapidity}");
 for(Int_t v=0;v<4;v++) // use weights [phi,pt,eta,rapidity]
 { 
  if(fUseWeights[v])fWeightsPro->Fill(v+0.5,1.);
 }
 fWeightsList->Add(fWeightsPro);

 fQvectorFlagsPro = new TProfile("fQvectorFlagsPro","flags for Q-vector objects",3,0.,3.);
 fQvectorFlagsPro->SetStats(kFALSE);
 fQvectorFlagsPro->GetXaxis()->SetLabelSize(0.05); 
 fQvectorFlagsPro->GetXaxis()->SetBinLabel(1,"fCalculateQvector");  
 fQvectorFlagsPro->Fill(0.5,fCalculateQvector);
 fQvectorFlagsPro->GetXaxis()->SetBinLabel(2,"fMaxHarmonic");  
 fQvectorFlagsPro->Fill(1.5,fMaxHarmonic);
 fQvectorFlagsPro->GetXaxis()->SetBinLabel(3,"fMaxCorrelator");  
 fQvectorFlagsPro->Fill(2.5,fMaxCorrelator);
 fQvectorList->Add(fQvectorFlagsPro);

 fCorrelationsFlagsPro = new TProfile("fCorrelationsFlagsPro","flags for multiparticle correlations",1,0.,1.);
 fCorrelationsFlagsPro->SetStats(kFALSE);
 fCorrelationsFlagsPro->GetXaxis()->SetLabelSize(0.05); 
 fCorrelationsFlagsPro->GetXaxis()->SetBinLabel(1,"fCalculateCorrelations");  
 fCorrelationsFlagsPro->Fill(0.5,fCalculateCorrelations);
 fCorrelationsList->Add(fCorrelationsFlagsPro);

 fNestedLoopsFlagsPro = new TProfile("fNestedLoopsFlagsPro","flags for nested loops",1,0.,1.);
 fNestedLoopsFlagsPro->SetStats(kFALSE);
 fNestedLoopsFlagsPro->GetXaxis()->SetLabelSize(0.05); 
 fNestedLoopsFlagsPro->GetXaxis()->SetBinLabel(1,"fCalculateNestedLoops");  
 fNestedLoopsFlagsPro->Fill(0.5,fCalculateNestedLoops);
 fNestedLoopsList->Add(fNestedLoopsFlagsPro);

 fOnTheFlyFlagsPro = new TProfile("fOnTheFlyFlagsPro","flags for on-the-fly",12,0.,12.);
 fOnTheFlyFlagsPro->SetStats(kFALSE);
 fOnTheFlyFlagsPro->SetLineColor(kBlack);
 fOnTheFlyFlagsPro->SetFillColor(kGray);
 fOnTheFlyFlagsPro->GetXaxis()->SetLabelSize(0.05); 
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(1,"fCalculateOnTheFly");  
 fOnTheFlyFlagsPro->Fill(0.5,fCalculateOnTheFly);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(2,"fUseDefaultPDFs");  
 fOnTheFlyFlagsPro->Fill(1.5,fUseDefaultPDFs);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(3,"fMass");  
 fOnTheFlyFlagsPro->Fill(2.5,fMass);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(4,"fTemperature");  
 fOnTheFlyFlagsPro->Fill(3.5,fTemperature);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(5,"fV1");  
 fOnTheFlyFlagsPro->Fill(4.5,fV1);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(6,"fV2");  
 fOnTheFlyFlagsPro->Fill(5.5,fV2);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(7,"fV3");  
 fOnTheFlyFlagsPro->Fill(6.5,fV3);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(8,"fV4");  
 fOnTheFlyFlagsPro->Fill(7.5,fV4);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(9,"fV5");  
 fOnTheFlyFlagsPro->Fill(8.5,fV5);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(10,"fV6");  
 fOnTheFlyFlagsPro->Fill(9.5,fV6);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(11,"fRandomSeed");  
 fOnTheFlyFlagsPro->Fill(10.5,fRandomSeed);
 fOnTheFlyFlagsPro->GetXaxis()->SetBinLabel(12,"fnEvents");  
 fOnTheFlyFlagsPro->Fill(11.5,fnEvents);

 fOnTheFlyList->Add(fOnTheFlyFlagsPro);

} // void FlowWithMultiparticleCorrelations::BookAllProfilesHoldingFlags()

//=======================================================================================================================

Float_t FlowWithMultiparticleCorrelations::Weight(const Float_t &value, const char *variable) // value, [phi,pt,eta,rapidity]
{
 // Determine particle weight. 

 // Basic protection:
 if(!(TString(variable).EqualTo("phi") || TString(variable).EqualTo("pt") || TString(variable).EqualTo("eta") || TString(variable).EqualTo("rapidity")))
 {
  cout<<__LINE__<<endl; exit(0);
 }

 Int_t pper = 0; // [phi,pt,eta,rapidity]
 if(TString(variable).EqualTo("pt")){pper=1;} 
 if(TString(variable).EqualTo("eta")){pper=2;} 
 if(TString(variable).EqualTo("rapidity")){pper=3;} 

 if(!fWeightsHist[pper]){cout<<__LINE__<<endl; exit(0);}

 Float_t weight = fWeightsHist[pper]->GetBinContent(fWeightsHist[pper]->FindBin(value)); // TBI_20200612 validate this line again

 return weight;

} // FlowWithMultiparticleCorrelations::Weight(const Float_t &value, const char *variable) // value, [phi,pt,eta,rapidity]

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Q(Int_t n, Int_t wp)
{
 // Using the fact that Q{-n,p} = Q{n,p}^*. 
 
 if(n>=0){return fQvector[n][wp];} 
 return TComplex::Conjugate(fQvector[-n][wp]);
 
} // TComplex FlowWithMultiparticleCorrelations::Q(Int_t n, Int_t wp)


//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::One(Int_t n1)
{
 // Generic expression <exp[i(n1*phi1)]>. TBI comment

 TComplex one = Q(n1,1);

 return one;

} // TComplex FlowWithMultiparticleCorrelations::One(Int_t n1)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Two(Int_t n1, Int_t n2)
{
 // Generic two-particle correlation <exp[i(n1*phi1+n2*phi2)]>.

 TComplex two = Q(n1,1)*Q(n2,1)-Q(n1+n2,2);

 return two;

} // TComplex FlowWithMultiparticleCorrelations::Two(Int_t n1, Int_t n2)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Three(Int_t n1, Int_t n2, Int_t n3)
{
 // Generic three-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3)]>.

 TComplex three = Q(n1,1)*Q(n2,1)*Q(n3,1)-Q(n1+n2,2)*Q(n3,1)-Q(n2,1)*Q(n1+n3,2)
                - Q(n1,1)*Q(n2+n3,2)+2.*Q(n1+n2+n3,3); 

 return three;

} // TComplex FlowWithMultiparticleCorrelations::Three(Int_t n1, Int_t n2, Int_t n3)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Four(Int_t n1, Int_t n2, Int_t n3, Int_t n4)
{
 // Generic four-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3+n4*phi4)]>.

 TComplex four = Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4,1)-Q(n1+n2,2)*Q(n3,1)*Q(n4,1)-Q(n2,1)*Q(n1+n3,2)*Q(n4,1)
               - Q(n1,1)*Q(n2+n3,2)*Q(n4,1)+2.*Q(n1+n2+n3,3)*Q(n4,1)-Q(n2,1)*Q(n3,1)*Q(n1+n4,2)
               + Q(n2+n3,2)*Q(n1+n4,2)-Q(n1,1)*Q(n3,1)*Q(n2+n4,2)+Q(n1+n3,2)*Q(n2+n4,2)
               + 2.*Q(n3,1)*Q(n1+n2+n4,3)-Q(n1,1)*Q(n2,1)*Q(n3+n4,2)+Q(n1+n2,2)*Q(n3+n4,2)
               + 2.*Q(n2,1)*Q(n1+n3+n4,3)+2.*Q(n1,1)*Q(n2+n3+n4,3)-6.*Q(n1+n2+n3+n4,4);

 return four;

} // TComplex FlowWithMultiparticleCorrelations::Four(Int_t n1, Int_t n2, Int_t n3, Int_t n4)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Five(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5)
{
 // Generic five-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3+n4*phi4+n5*phi5)]>.

 TComplex five = Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n5,1)-Q(n1+n2,2)*Q(n3,1)*Q(n4,1)*Q(n5,1)
               - Q(n2,1)*Q(n1+n3,2)*Q(n4,1)*Q(n5,1)-Q(n1,1)*Q(n2+n3,2)*Q(n4,1)*Q(n5,1)
               + 2.*Q(n1+n2+n3,3)*Q(n4,1)*Q(n5,1)-Q(n2,1)*Q(n3,1)*Q(n1+n4,2)*Q(n5,1)
               + Q(n2+n3,2)*Q(n1+n4,2)*Q(n5,1)-Q(n1,1)*Q(n3,1)*Q(n2+n4,2)*Q(n5,1)
               + Q(n1+n3,2)*Q(n2+n4,2)*Q(n5,1)+2.*Q(n3,1)*Q(n1+n2+n4,3)*Q(n5,1)
               - Q(n1,1)*Q(n2,1)*Q(n3+n4,2)*Q(n5,1)+Q(n1+n2,2)*Q(n3+n4,2)*Q(n5,1)
               + 2.*Q(n2,1)*Q(n1+n3+n4,3)*Q(n5,1)+2.*Q(n1,1)*Q(n2+n3+n4,3)*Q(n5,1)
               - 6.*Q(n1+n2+n3+n4,4)*Q(n5,1)-Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n1+n5,2)
               + Q(n2+n3,2)*Q(n4,1)*Q(n1+n5,2)+Q(n3,1)*Q(n2+n4,2)*Q(n1+n5,2)
               + Q(n2,1)*Q(n3+n4,2)*Q(n1+n5,2)-2.*Q(n2+n3+n4,3)*Q(n1+n5,2)
               - Q(n1,1)*Q(n3,1)*Q(n4,1)*Q(n2+n5,2)+Q(n1+n3,2)*Q(n4,1)*Q(n2+n5,2)
               + Q(n3,1)*Q(n1+n4,2)*Q(n2+n5,2)+Q(n1,1)*Q(n3+n4,2)*Q(n2+n5,2)
               - 2.*Q(n1+n3+n4,3)*Q(n2+n5,2)+2.*Q(n3,1)*Q(n4,1)*Q(n1+n2+n5,3)
               - 2.*Q(n3+n4,2)*Q(n1+n2+n5,3)-Q(n1,1)*Q(n2,1)*Q(n4,1)*Q(n3+n5,2)
               + Q(n1+n2,2)*Q(n4,1)*Q(n3+n5,2)+Q(n2,1)*Q(n1+n4,2)*Q(n3+n5,2)
               + Q(n1,1)*Q(n2+n4,2)*Q(n3+n5,2)-2.*Q(n1+n2+n4,3)*Q(n3+n5,2)
               + 2.*Q(n2,1)*Q(n4,1)*Q(n1+n3+n5,3)-2.*Q(n2+n4,2)*Q(n1+n3+n5,3)
               + 2.*Q(n1,1)*Q(n4,1)*Q(n2+n3+n5,3)-2.*Q(n1+n4,2)*Q(n2+n3+n5,3)
               - 6.*Q(n4,1)*Q(n1+n2+n3+n5,4)-Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4+n5,2)
               + Q(n1+n2,2)*Q(n3,1)*Q(n4+n5,2)+Q(n2,1)*Q(n1+n3,2)*Q(n4+n5,2)
               + Q(n1,1)*Q(n2+n3,2)*Q(n4+n5,2)-2.*Q(n1+n2+n3,3)*Q(n4+n5,2)
               + 2.*Q(n2,1)*Q(n3,1)*Q(n1+n4+n5,3)-2.*Q(n2+n3,2)*Q(n1+n4+n5,3)
               + 2.*Q(n1,1)*Q(n3,1)*Q(n2+n4+n5,3)-2.*Q(n1+n3,2)*Q(n2+n4+n5,3)
               - 6.*Q(n3,1)*Q(n1+n2+n4+n5,4)+2.*Q(n1,1)*Q(n2,1)*Q(n3+n4+n5,3) 
               - 2.*Q(n1+n2,2)*Q(n3+n4+n5,3)-6.*Q(n2,1)*Q(n1+n3+n4+n5,4)
               - 6.*Q(n1,1)*Q(n2+n3+n4+n5,4)+24.*Q(n1+n2+n3+n4+n5,5);
 
 return five;

} // TComplex FlowWithMultiparticleCorrelations::Five(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Six(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6)
{
 // Generic six-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3+n4*phi4+n5*phi5+n6*phi6)]>.

 TComplex six = Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n5,1)*Q(n6,1)-Q(n1+n2,2)*Q(n3,1)*Q(n4,1)*Q(n5,1)*Q(n6,1)
              - Q(n2,1)*Q(n1+n3,2)*Q(n4,1)*Q(n5,1)*Q(n6,1)-Q(n1,1)*Q(n2+n3,2)*Q(n4,1)*Q(n5,1)*Q(n6,1)
              + 2.*Q(n1+n2+n3,3)*Q(n4,1)*Q(n5,1)*Q(n6,1)-Q(n2,1)*Q(n3,1)*Q(n1+n4,2)*Q(n5,1)*Q(n6,1)
              + Q(n2+n3,2)*Q(n1+n4,2)*Q(n5,1)*Q(n6,1)-Q(n1,1)*Q(n3,1)*Q(n2+n4,2)*Q(n5,1)*Q(n6,1)
              + Q(n1+n3,2)*Q(n2+n4,2)*Q(n5,1)*Q(n6,1)+2.*Q(n3,1)*Q(n1+n2+n4,3)*Q(n5,1)*Q(n6,1)
              - Q(n1,1)*Q(n2,1)*Q(n3+n4,2)*Q(n5,1)*Q(n6,1)+Q(n1+n2,2)*Q(n3+n4,2)*Q(n5,1)*Q(n6,1)
              + 2.*Q(n2,1)*Q(n1+n3+n4,3)*Q(n5,1)*Q(n6,1)+2.*Q(n1,1)*Q(n2+n3+n4,3)*Q(n5,1)*Q(n6,1)
              - 6.*Q(n1+n2+n3+n4,4)*Q(n5,1)*Q(n6,1)-Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n1+n5,2)*Q(n6,1)
              + Q(n2+n3,2)*Q(n4,1)*Q(n1+n5,2)*Q(n6,1)+Q(n3,1)*Q(n2+n4,2)*Q(n1+n5,2)*Q(n6,1)
              + Q(n2,1)*Q(n3+n4,2)*Q(n1+n5,2)*Q(n6,1)-2.*Q(n2+n3+n4,3)*Q(n1+n5,2)*Q(n6,1)
              - Q(n1,1)*Q(n3,1)*Q(n4,1)*Q(n2+n5,2)*Q(n6,1)+Q(n1+n3,2)*Q(n4,1)*Q(n2+n5,2)*Q(n6,1)
              + Q(n3,1)*Q(n1+n4,2)*Q(n2+n5,2)*Q(n6,1)+Q(n1,1)*Q(n3+n4,2)*Q(n2+n5,2)*Q(n6,1)
              - 2.*Q(n1+n3+n4,3)*Q(n2+n5,2)*Q(n6,1)+2.*Q(n3,1)*Q(n4,1)*Q(n1+n2+n5,3)*Q(n6,1)
              - 2.*Q(n3+n4,2)*Q(n1+n2+n5,3)*Q(n6,1)-Q(n1,1)*Q(n2,1)*Q(n4,1)*Q(n3+n5,2)*Q(n6,1)
              + Q(n1+n2,2)*Q(n4,1)*Q(n3+n5,2)*Q(n6,1)+Q(n2,1)*Q(n1+n4,2)*Q(n3+n5,2)*Q(n6,1)
              + Q(n1,1)*Q(n2+n4,2)*Q(n3+n5,2)*Q(n6,1)-2.*Q(n1+n2+n4,3)*Q(n3+n5,2)*Q(n6,1)
              + 2.*Q(n2,1)*Q(n4,1)*Q(n1+n3+n5,3)*Q(n6,1)-2.*Q(n2+n4,2)*Q(n1+n3+n5,3)*Q(n6,1)
              + 2.*Q(n1,1)*Q(n4,1)*Q(n2+n3+n5,3)*Q(n6,1)-2.*Q(n1+n4,2)*Q(n2+n3+n5,3)*Q(n6,1)
              - 6.*Q(n4,1)*Q(n1+n2+n3+n5,4)*Q(n6,1)-Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4+n5,2)*Q(n6,1)
              + Q(n1+n2,2)*Q(n3,1)*Q(n4+n5,2)*Q(n6,1)+Q(n2,1)*Q(n1+n3,2)*Q(n4+n5,2)*Q(n6,1)
              + Q(n1,1)*Q(n2+n3,2)*Q(n4+n5,2)*Q(n6,1)-2.*Q(n1+n2+n3,3)*Q(n4+n5,2)*Q(n6,1)
              + 2.*Q(n2,1)*Q(n3,1)*Q(n1+n4+n5,3)*Q(n6,1)-2.*Q(n2+n3,2)*Q(n1+n4+n5,3)*Q(n6,1)
              + 2.*Q(n1,1)*Q(n3,1)*Q(n2+n4+n5,3)*Q(n6,1)-2.*Q(n1+n3,2)*Q(n2+n4+n5,3)*Q(n6,1)
              - 6.*Q(n3,1)*Q(n1+n2+n4+n5,4)*Q(n6,1)+2.*Q(n1,1)*Q(n2,1)*Q(n3+n4+n5,3)*Q(n6,1)
              - 2.*Q(n1+n2,2)*Q(n3+n4+n5,3)*Q(n6,1)-6.*Q(n2,1)*Q(n1+n3+n4+n5,4)*Q(n6,1)
              - 6.*Q(n1,1)*Q(n2+n3+n4+n5,4)*Q(n6,1)+24.*Q(n1+n2+n3+n4+n5,5)*Q(n6,1)
              - Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n5,1)*Q(n1+n6,2)+Q(n2+n3,2)*Q(n4,1)*Q(n5,1)*Q(n1+n6,2)
              + Q(n3,1)*Q(n2+n4,2)*Q(n5,1)*Q(n1+n6,2)+Q(n2,1)*Q(n3+n4,2)*Q(n5,1)*Q(n1+n6,2)
              - 2.*Q(n2+n3+n4,3)*Q(n5,1)*Q(n1+n6,2)+Q(n3,1)*Q(n4,1)*Q(n2+n5,2)*Q(n1+n6,2)
              - Q(n3+n4,2)*Q(n2+n5,2)*Q(n1+n6,2)+Q(n2,1)*Q(n4,1)*Q(n3+n5,2)*Q(n1+n6,2)
              - Q(n2+n4,2)*Q(n3+n5,2)*Q(n1+n6,2)-2.*Q(n4,1)*Q(n2+n3+n5,3)*Q(n1+n6,2)
              + Q(n2,1)*Q(n3,1)*Q(n4+n5,2)*Q(n1+n6,2)-Q(n2+n3,2)*Q(n4+n5,2)*Q(n1+n6,2)
              - 2.*Q(n3,1)*Q(n2+n4+n5,3)*Q(n1+n6,2)-2.*Q(n2,1)*Q(n3+n4+n5,3)*Q(n1+n6,2)
              + 6.*Q(n2+n3+n4+n5,4)*Q(n1+n6,2)-Q(n1,1)*Q(n3,1)*Q(n4,1)*Q(n5,1)*Q(n2+n6,2)
              + Q(n1+n3,2)*Q(n4,1)*Q(n5,1)*Q(n2+n6,2)+Q(n3,1)*Q(n1+n4,2)*Q(n5,1)*Q(n2+n6,2)
              + Q(n1,1)*Q(n3+n4,2)*Q(n5,1)*Q(n2+n6,2)-2.*Q(n1+n3+n4,3)*Q(n5,1)*Q(n2+n6,2)
              + Q(n3,1)*Q(n4,1)*Q(n1+n5,2)*Q(n2+n6,2)-Q(n3+n4,2)*Q(n1+n5,2)*Q(n2+n6,2)
              + Q(n1,1)*Q(n4,1)*Q(n3+n5,2)*Q(n2+n6,2)-Q(n1+n4,2)*Q(n3+n5,2)*Q(n2+n6,2)
              - 2.*Q(n4,1)*Q(n1+n3+n5,3)*Q(n2+n6,2)+Q(n1,1)*Q(n3,1)*Q(n4+n5,2)*Q(n2+n6,2)
              - Q(n1+n3,2)*Q(n4+n5,2)*Q(n2+n6,2)-2.*Q(n3,1)*Q(n1+n4+n5,3)*Q(n2+n6,2)
              - 2.*Q(n1,1)*Q(n3+n4+n5,3)*Q(n2+n6,2)+6.*Q(n1+n3+n4+n5,4)*Q(n2+n6,2)
              + 2.*Q(n3,1)*Q(n4,1)*Q(n5,1)*Q(n1+n2+n6,3)-2.*Q(n3+n4,2)*Q(n5,1)*Q(n1+n2+n6,3)
              - 2.*Q(n4,1)*Q(n3+n5,2)*Q(n1+n2+n6,3)-2.*Q(n3,1)*Q(n4+n5,2)*Q(n1+n2+n6,3)
              + 4.*Q(n3+n4+n5,3)*Q(n1+n2+n6,3)-Q(n1,1)*Q(n2,1)*Q(n4,1)*Q(n5,1)*Q(n3+n6,2)
              + Q(n1+n2,2)*Q(n4,1)*Q(n5,1)*Q(n3+n6,2)+Q(n2,1)*Q(n1+n4,2)*Q(n5,1)*Q(n3+n6,2)
              + Q(n1,1)*Q(n2+n4,2)*Q(n5,1)*Q(n3+n6,2)-2.*Q(n1+n2+n4,3)*Q(n5,1)*Q(n3+n6,2)
              + Q(n2,1)*Q(n4,1)*Q(n1+n5,2)*Q(n3+n6,2)-Q(n2+n4,2)*Q(n1+n5,2)*Q(n3+n6,2)
              + Q(n1,1)*Q(n4,1)*Q(n2+n5,2)*Q(n3+n6,2)-Q(n1+n4,2)*Q(n2+n5,2)*Q(n3+n6,2)
              - 2.*Q(n4,1)*Q(n1+n2+n5,3)*Q(n3+n6,2)+Q(n1,1)*Q(n2,1)*Q(n4+n5,2)*Q(n3+n6,2)
              - Q(n1+n2,2)*Q(n4+n5,2)*Q(n3+n6,2)-2.*Q(n2,1)*Q(n1+n4+n5,3)*Q(n3+n6,2)
              - 2.*Q(n1,1)*Q(n2+n4+n5,3)*Q(n3+n6,2)+6.*Q(n1+n2+n4+n5,4)*Q(n3+n6,2)
              + 2.*Q(n2,1)*Q(n4,1)*Q(n5,1)*Q(n1+n3+n6,3)-2.*Q(n2+n4,2)*Q(n5,1)*Q(n1+n3+n6,3)
              - 2.*Q(n4,1)*Q(n2+n5,2)*Q(n1+n3+n6,3)-2.*Q(n2,1)*Q(n4+n5,2)*Q(n1+n3+n6,3)
              + 4.*Q(n2+n4+n5,3)*Q(n1+n3+n6,3)+2.*Q(n1,1)*Q(n4,1)*Q(n5,1)*Q(n2+n3+n6,3)
              - 2.*Q(n1+n4,2)*Q(n5,1)*Q(n2+n3+n6,3)-2.*Q(n4,1)*Q(n1+n5,2)*Q(n2+n3+n6,3)
              - 2.*Q(n1,1)*Q(n4+n5,2)*Q(n2+n3+n6,3)+4.*Q(n1+n4+n5,3)*Q(n2+n3+n6,3)
              - 6.*Q(n4,1)*Q(n5,1)*Q(n1+n2+n3+n6,4)+6.*Q(n4+n5,2)*Q(n1+n2+n3+n6,4)
              - Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n5,1)*Q(n4+n6,2)+Q(n1+n2,2)*Q(n3,1)*Q(n5,1)*Q(n4+n6,2)
              + Q(n2,1)*Q(n1+n3,2)*Q(n5,1)*Q(n4+n6,2)+Q(n1,1)*Q(n2+n3,2)*Q(n5,1)*Q(n4+n6,2)
              - 2.*Q(n1+n2+n3,3)*Q(n5,1)*Q(n4+n6,2)+Q(n2,1)*Q(n3,1)*Q(n1+n5,2)*Q(n4+n6,2)
              - Q(n2+n3,2)*Q(n1+n5,2)*Q(n4+n6,2)+Q(n1,1)*Q(n3,1)*Q(n2+n5,2)*Q(n4+n6,2)
              - Q(n1+n3,2)*Q(n2+n5,2)*Q(n4+n6,2)-2.*Q(n3,1)*Q(n1+n2+n5,3)*Q(n4+n6,2)
              + Q(n1,1)*Q(n2,1)*Q(n3+n5,2)*Q(n4+n6,2)-Q(n1+n2,2)*Q(n3+n5,2)*Q(n4+n6,2)
              - 2.*Q(n2,1)*Q(n1+n3+n5,3)*Q(n4+n6,2)-2.*Q(n1,1)*Q(n2+n3+n5,3)*Q(n4+n6,2)
              + 6.*Q(n1+n2+n3+n5,4)*Q(n4+n6,2)+2.*Q(n2,1)*Q(n3,1)*Q(n5,1)*Q(n1+n4+n6,3)
              - 2.*Q(n2+n3,2)*Q(n5,1)*Q(n1+n4+n6,3)-2.*Q(n3,1)*Q(n2+n5,2)*Q(n1+n4+n6,3)
              - 2.*Q(n2,1)*Q(n3+n5,2)*Q(n1+n4+n6,3)+4.*Q(n2+n3+n5,3)*Q(n1+n4+n6,3)
              + 2.*Q(n1,1)*Q(n3,1)*Q(n5,1)*Q(n2+n4+n6,3)-2.*Q(n1+n3,2)*Q(n5,1)*Q(n2+n4+n6,3)
              - 2.*Q(n3,1)*Q(n1+n5,2)*Q(n2+n4+n6,3)-2.*Q(n1,1)*Q(n3+n5,2)*Q(n2+n4+n6,3)
              + 4.*Q(n1+n3+n5,3)*Q(n2+n4+n6,3)-6.*Q(n3,1)*Q(n5,1)*Q(n1+n2+n4+n6,4)
              + 6.*Q(n3+n5,2)*Q(n1+n2+n4+n6,4)+2.*Q(n1,1)*Q(n2,1)*Q(n5,1)*Q(n3+n4+n6,3)
              - 2.*Q(n1+n2,2)*Q(n5,1)*Q(n3+n4+n6,3)-2.*Q(n2,1)*Q(n1+n5,2)*Q(n3+n4+n6,3)
              - 2.*Q(n1,1)*Q(n2+n5,2)*Q(n3+n4+n6,3)+4.*Q(n1+n2+n5,3)*Q(n3+n4+n6,3)
              - 6.*Q(n2,1)*Q(n5,1)*Q(n1+n3+n4+n6,4)+6.*Q(n2+n5,2)*Q(n1+n3+n4+n6,4)
              - 6.*Q(n1,1)*Q(n5,1)*Q(n2+n3+n4+n6,4)+6.*Q(n1+n5,2)*Q(n2+n3+n4+n6,4)
              + 24.*Q(n5,1)*Q(n1+n2+n3+n4+n6,5)-Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n5+n6,2)
              + Q(n1+n2,2)*Q(n3,1)*Q(n4,1)*Q(n5+n6,2)+Q(n2,1)*Q(n1+n3,2)*Q(n4,1)*Q(n5+n6,2)
              + Q(n1,1)*Q(n2+n3,2)*Q(n4,1)*Q(n5+n6,2)-2.*Q(n1+n2+n3,3)*Q(n4,1)*Q(n5+n6,2)
              + Q(n2,1)*Q(n3,1)*Q(n1+n4,2)*Q(n5+n6,2)-Q(n2+n3,2)*Q(n1+n4,2)*Q(n5+n6,2)
              + Q(n1,1)*Q(n3,1)*Q(n2+n4,2)*Q(n5+n6,2)-Q(n1+n3,2)*Q(n2+n4,2)*Q(n5+n6,2)
              - 2.*Q(n3,1)*Q(n1+n2+n4,3)*Q(n5+n6,2)+Q(n1,1)*Q(n2,1)*Q(n3+n4,2)*Q(n5+n6,2)
              - Q(n1+n2,2)*Q(n3+n4,2)*Q(n5+n6,2)-2.*Q(n2,1)*Q(n1+n3+n4,3)*Q(n5+n6,2)
              - 2.*Q(n1,1)*Q(n2+n3+n4,3)*Q(n5+n6,2)+6.*Q(n1+n2+n3+n4,4)*Q(n5+n6,2)
              + 2.*Q(n2,1)*Q(n3,1)*Q(n4,1)*Q(n1+n5+n6,3)-2.*Q(n2+n3,2)*Q(n4,1)*Q(n1+n5+n6,3)
              - 2.*Q(n3,1)*Q(n2+n4,2)*Q(n1+n5+n6,3)-2.*Q(n2,1)*Q(n3+n4,2)*Q(n1+n5+n6,3)
              + 4.*Q(n2+n3+n4,3)*Q(n1+n5+n6,3)+2.*Q(n1,1)*Q(n3,1)*Q(n4,1)*Q(n2+n5+n6,3)
              - 2.*Q(n1+n3,2)*Q(n4,1)*Q(n2+n5+n6,3)-2.*Q(n3,1)*Q(n1+n4,2)*Q(n2+n5+n6,3)
              - 2.*Q(n1,1)*Q(n3+n4,2)*Q(n2+n5+n6,3)+4.*Q(n1+n3+n4,3)*Q(n2+n5+n6,3)
              - 6.*Q(n3,1)*Q(n4,1)*Q(n1+n2+n5+n6,4)+6.*Q(n3+n4,2)*Q(n1+n2+n5+n6,4)
              + 2.*Q(n1,1)*Q(n2,1)*Q(n4,1)*Q(n3+n5+n6,3)-2.*Q(n1+n2,2)*Q(n4,1)*Q(n3+n5+n6,3)
              - 2.*Q(n2,1)*Q(n1+n4,2)*Q(n3+n5+n6,3)-2.*Q(n1,1)*Q(n2+n4,2)*Q(n3+n5+n6,3)
              + 4.*Q(n1+n2+n4,3)*Q(n3+n5+n6,3)-6.*Q(n2,1)*Q(n4,1)*Q(n1+n3+n5+n6,4)
              + 6.*Q(n2+n4,2)*Q(n1+n3+n5+n6,4)-6.*Q(n1,1)*Q(n4,1)*Q(n2+n3+n5+n6,4)
              + 6.*Q(n1+n4,2)*Q(n2+n3+n5+n6,4)+24.*Q(n4,1)*Q(n1+n2+n3+n5+n6,5)
              + 2.*Q(n1,1)*Q(n2,1)*Q(n3,1)*Q(n4+n5+n6,3)-2.*Q(n1+n2,2)*Q(n3,1)*Q(n4+n5+n6,3)
              - 2.*Q(n2,1)*Q(n1+n3,2)*Q(n4+n5+n6,3)-2.*Q(n1,1)*Q(n2+n3,2)*Q(n4+n5+n6,3)
              + 4.*Q(n1+n2+n3,3)*Q(n4+n5+n6,3)-6.*Q(n2,1)*Q(n3,1)*Q(n1+n4+n5+n6,4)
              + 6.*Q(n2+n3,2)*Q(n1+n4+n5+n6,4)-6.*Q(n1,1)*Q(n3,1)*Q(n2+n4+n5+n6,4)
              + 6.*Q(n1+n3,2)*Q(n2+n4+n5+n6,4)+24.*Q(n3,1)*Q(n1+n2+n4+n5+n6,5)
              - 6.*Q(n1,1)*Q(n2,1)*Q(n3+n4+n5+n6,4)+6.*Q(n1+n2,2)*Q(n3+n4+n5+n6,4)
              + 24.*Q(n2,1)*Q(n1+n3+n4+n5+n6,5)+24.*Q(n1,1)*Q(n2+n3+n4+n5+n6,5)
              - 120.*Q(n1+n2+n3+n4+n5+n6,6);

 return six;

} // TComplex FlowWithMultiparticleCorrelations::Six(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Seven(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7)
{
 // Generic seven-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3+n4*phi4+n5*phi5+n6*phi6+n7*phi7)]>.

 Int_t harmonic[7] = {n1,n2,n3,n4,n5,n6,n7};

 TComplex seven = Recursion(7,harmonic); 

 return seven;

} // end of TComplex FlowWithMultiparticleCorrelations::Seven(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Eight(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7, Int_t n8)
{
 // Generic eight-particle correlation <exp[i(n1*phi1+n2*phi2+n3*phi3+n4*phi4+n5*phi5+n6*phi6+n7*phi7+n8*phi8)]>.

 Int_t harmonic[8] = {n1,n2,n3,n4,n5,n6,n7,n8};

 TComplex eight = Recursion(8,harmonic); 

 return eight;

} // end of TComplex FlowWithMultiparticleCorrelations::Eight(Int_t n1, Int_t n2, Int_t n3, Int_t n4, Int_t n5, Int_t n6, Int_t n7, Int_t n8)

//=======================================================================================================================

TComplex FlowWithMultiparticleCorrelations::Recursion(Int_t n, Int_t* harmonic, Int_t mult, Int_t skip) 
{
 // Calculate multi-particle correlators by using recursion (an improved faster version) originally developed by 
 // Kristjan Gulbrandsen (gulbrand@nbi.dk). 

  Int_t nm1 = n-1;
  TComplex c(Q(harmonic[nm1], mult));
  if (nm1 == 0) return c;
  c *= Recursion(nm1, harmonic);
  if (nm1 == skip) return c;

  Int_t multp1 = mult+1;
  Int_t nm2 = n-2;
  Int_t counter1 = 0;
  Int_t hhold = harmonic[counter1];
  harmonic[counter1] = harmonic[nm2];
  harmonic[nm2] = hhold + harmonic[nm1];
  TComplex c2(Recursion(nm1, harmonic, multp1, nm2));
  Int_t counter2 = n-3;
  while (counter2 >= skip) {
    harmonic[nm2] = harmonic[counter1];
    harmonic[counter1] = hhold;
    ++counter1;
    hhold = harmonic[counter1];
    harmonic[counter1] = harmonic[nm2];
    harmonic[nm2] = hhold + harmonic[nm1];
    c2 += Recursion(nm1, harmonic, multp1, counter2);
    --counter2;
  }
  harmonic[nm2] = harmonic[counter1];
  harmonic[counter1] = hhold;

  if (mult == 1) return c-c2;
  return c-Double_t(mult)*c2;

} // TComplex FlowWithMultiparticleCorrelations::Recursion(Int_t n, Int_t* harmonic, Int_t mult, Int_t skip) 

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::CalculateCorrelations(void) 
{
 // Calculate analytically multiparticle correlations from Q-vectors.

 cout<<"CalculateCorrelations(void) : "<<fnSelectedTracksEBE<<" .... "<<endl;

 for(Int_t h=1;h<=fMaxHarmonic;h++) // harmonic
 {
  
  // 2p:
  //cout<<"   => CalculateCorrelations(void), 2p .... "<<endl;
  if(fnSelectedTracksEBE<2){return;}
  TComplex twoC = Two(h,-h)/Two(0,0).Re(); // cos
  TComplex twoS = Two(h,-h)/Two(0,0).Im(); // sin
  Double_t wTwo = Two(0,0).Re(); // Weight is 'number of combinations' by default TBI_20200612 add support for other weights
  // integrated:
  if(fCorrelationsPro[0][0][h-1][0]){fCorrelationsPro[0][0][h-1][0]->Fill(0.5,twoC,wTwo);}
  if(fCorrelationsPro[1][0][h-1][0]){fCorrelationsPro[1][0][h-1][0]->Fill(0.5,twoS,wTwo);}
  // vs. multiplicity:
  if(fCorrelationsPro[0][0][h-1][1]){fCorrelationsPro[0][0][h-1][1]->Fill(fnSelectedTracksEBE+0.5,twoC,wTwo);}
  if(fCorrelationsPro[1][0][h-1][1]){fCorrelationsPro[1][0][h-1][1]->Fill(fnSelectedTracksEBE+0.5,twoS,wTwo);}

  // 4p:
  //cout<<"   => CalculateCorrelations(void), 4p .... "<<endl;
  if(fnSelectedTracksEBE<4){return;}
  TComplex fourC = Four(h,h,-h,-h)/Four(0,0,0,0).Re(); // cos
  TComplex fourS = Four(h,h,-h,-h)/Four(0,0,0,0).Im(); // sin
  Double_t wFour = Four(0,0,0,0).Re(); // Weight is 'number of combinations' by default TBI_20200612 add support for other weights
  // integrated:
  if(fCorrelationsPro[0][1][h-1][0]){fCorrelationsPro[0][1][h-1][0]->Fill(0.5,fourC,wFour);}
  if(fCorrelationsPro[1][1][h-1][0]){fCorrelationsPro[1][1][h-1][0]->Fill(0.5,fourS,wFour);}
  // vs. multiplicity:
  if(fCorrelationsPro[0][1][h-1][1]){fCorrelationsPro[0][1][h-1][1]->Fill(fnSelectedTracksEBE+0.5,fourC,wFour);}
  if(fCorrelationsPro[1][1][h-1][1]){fCorrelationsPro[1][1][h-1][1]->Fill(fnSelectedTracksEBE+0.5,fourS,wFour);}

  // 6p:
  //cout<<"   => CalculateCorrelations(void), 6p .... "<<endl;
  if(fnSelectedTracksEBE<6){return;}
  TComplex sixC = Six(h,h,h,-h,-h,-h)/Six(0,0,0,0,0,0).Re(); // cos
  TComplex sixS = Six(h,h,h,-h,-h,-h)/Six(0,0,0,0,0,0).Im(); // sin
  Double_t wSix = Six(0,0,0,0,0,0).Re(); // Weight is 'number of combinations' by default TBI_20200612 add support for other weights
  // integrated:
  if(fCorrelationsPro[0][2][h-1][0]){fCorrelationsPro[0][2][h-1][0]->Fill(0.5,sixC,wSix);}
  if(fCorrelationsPro[1][2][h-1][0]){fCorrelationsPro[1][2][h-1][0]->Fill(0.5,sixS,wSix);}
  // vs. multiplicity:
  if(fCorrelationsPro[0][2][h-1][1]){fCorrelationsPro[0][2][h-1][1]->Fill(fnSelectedTracksEBE+0.5,sixC,wSix);}
  if(fCorrelationsPro[1][2][h-1][1]){fCorrelationsPro[1][2][h-1][1]->Fill(fnSelectedTracksEBE+0.5,sixS,wSix);}

  // 8p:
  //cout<<"   => CalculateCorrelations(void), 8p .... "<<endl;
  if(fnSelectedTracksEBE<8){return;}
  TComplex eightC = Eight(h,h,h,h,-h,-h,-h,-h)/Eight(0,0,0,0,0,0,0,0).Re(); // cos
  TComplex eightS = Eight(h,h,h,h,-h,-h,-h,-h)/Eight(0,0,0,0,0,0,0,0).Im(); // sin
  Double_t wEight = Eight(0,0,0,0,0,0,0,0).Re(); // Weight is 'number of combinations' by default TBI_20200612 add support for other weights
  // integrated:
  if(fCorrelationsPro[0][3][h-1][0]){fCorrelationsPro[0][3][h-1][0]->Fill(0.5,eightC,wEight);}
  if(fCorrelationsPro[1][3][h-1][0]){fCorrelationsPro[1][3][h-1][0]->Fill(0.5,eightS,wEight);}
  // vs. multiplicity:
  if(fCorrelationsPro[0][3][h-1][1]){fCorrelationsPro[0][3][h-1][1]->Fill(fnSelectedTracksEBE+0.5,eightC,wEight);}
  if(fCorrelationsPro[1][3][h-1][1]){fCorrelationsPro[1][3][h-1][1]->Fill(fnSelectedTracksEBE+0.5,eightS,wEight);}
 
 } // for(Int_t h=1;h<=fMaxHarmonic;h++) // harmonic

 //cout<<"      Done! .... "<<endl;

} // void FlowWithMultiparticleCorrelations::CalculateCorrelations(void)

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::CalculateNestedLoops(void) 
{
 // Calculate multiparticle correlations with nested loops. 

 cout<<"   => CalculateNestedLoops(void) : fnSelectedTracksEBE = "<<fnSelectedTracksEBE<<endl;

 // 2p:
 if(fnSelectedTracksEBE<2){return;}
 cout<<"      CalculateNestedLoops(void), 2-p correlations .... "<<endl;
 for(int i1=0; i1<fnSelectedTracksEBE; i1++)
 {
  Float_t dPhi1 = ftaNestedLoops[0]->GetAt(i1);
  Float_t dW1 = ftaNestedLoops[1]->GetAt(i1);
  for(int i2=0; i2<fnSelectedTracksEBE; i2++)
  {
   if(i2==i1){continue;}
   Float_t dPhi2 = ftaNestedLoops[0]->GetAt(i2);
   Float_t dW2 = ftaNestedLoops[1]->GetAt(i2);
   for(int h=0; h<6; h++)
   {
    // fill cos, 2p, integreated: 
    fNestedLoopsPro[0][0][h][0]->Fill(0.5,TMath::Cos((h+1.)*(dPhi1-dPhi2)),dW1*dW2);
    // fill cos, 2p, vs. M: 
    fNestedLoopsPro[0][0][h][1]->Fill(fnSelectedTracksEBE+0.5,TMath::Cos((h+1.)*(dPhi1-dPhi2)),dW1*dW2);
   } // for(int h=1; h<=6; h++)
  } // for(int i2=0; i2<nTracks; ++i2)
 } // for(int i1=0; i1<nTracks; ++i1)

 // 4p:
 if(fnSelectedTracksEBE<4){return;}
 cout<<"      CalculateNestedLoops(void), 4-p correlations .... "<<endl;
 for(int i1=0; i1<fnSelectedTracksEBE; i1++)
 {
  Float_t dPhi1 = ftaNestedLoops[0]->GetAt(i1);
  Float_t dW1 = ftaNestedLoops[1]->GetAt(i1);
  for(int i2=0; i2<fnSelectedTracksEBE; i2++)
  {
   if(i2==i1){continue;}
   Float_t dPhi2 = ftaNestedLoops[0]->GetAt(i2);
   Float_t dW2 = ftaNestedLoops[1]->GetAt(i2);
   for(int i3=0; i3<fnSelectedTracksEBE; i3++)
   {
    if(i3==i1||i3==i2){continue;}
    Float_t dPhi3 = ftaNestedLoops[0]->GetAt(i3);
    Float_t dW3 = ftaNestedLoops[1]->GetAt(i3);
    for(int i4=0; i4<fnSelectedTracksEBE; i4++)
    {
     if(i4==i1||i4==i2||i4==i3){continue;}
     Float_t dPhi4 = ftaNestedLoops[0]->GetAt(i4);
     Float_t dW4 = ftaNestedLoops[1]->GetAt(i4);
     for(int h=0; h<6; h++)
     {
      // fill cos, 4p, integreated: 
      fNestedLoopsPro[0][1][h][0]->Fill(0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2-dPhi3-dPhi4)),dW1*dW2*dW3*dW4);
      // fill cos, 4p, all harmonics, vs. M: 
      fNestedLoopsPro[0][1][h][1]->Fill(fnSelectedTracksEBE+0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2-dPhi3-dPhi4)),dW1*dW2*dW3*dW4);
     } // for(int h=0; h<6; h++)
    } // for(int i4=0; i4<fnSelectedTracksEBE; i4++)   
   } // for(int i3=0; i3<fnSelectedTracksEBE; i3++)
  } // for(int i2=0; i2<nTracks; ++i2)
 } // for(int i1=0; i1<nTracks; ++i1)

 // 6p:
 if(fnSelectedTracksEBE<6){return;}
 cout<<"      CalculateNestedLoops(void), 6-p correlations .... "<<endl;
 for(int i1=0; i1<fnSelectedTracksEBE; i1++)
 {
  Float_t dPhi1 = ftaNestedLoops[0]->GetAt(i1);
  Float_t dW1 = ftaNestedLoops[1]->GetAt(i1);
  for(int i2=0; i2<fnSelectedTracksEBE; i2++)
  {
   if(i2==i1){continue;}
   Float_t dPhi2 = ftaNestedLoops[0]->GetAt(i2);
   Float_t dW2 = ftaNestedLoops[1]->GetAt(i2);
   for(int i3=0; i3<fnSelectedTracksEBE; i3++)
   {
    if(i3==i1||i3==i2){continue;}
    Float_t dPhi3 = ftaNestedLoops[0]->GetAt(i3);
    Float_t dW3 = ftaNestedLoops[1]->GetAt(i3);
    for(int i4=0; i4<fnSelectedTracksEBE; i4++)
    {
     if(i4==i1||i4==i2||i4==i3){continue;}
     Float_t dPhi4 = ftaNestedLoops[0]->GetAt(i4);
     Float_t dW4 = ftaNestedLoops[1]->GetAt(i4);
     for(int i5=0; i5<fnSelectedTracksEBE; i5++)
     {
      if(i5==i1||i5==i2||i5==i3||i5==i4){continue;}
      Float_t dPhi5 = ftaNestedLoops[0]->GetAt(i5);
      Float_t dW5 = ftaNestedLoops[1]->GetAt(i5);
      for(int i6=0; i6<fnSelectedTracksEBE; i6++)
      {
       if(i6==i1||i6==i2||i6==i3||i6==i4||i6==i5){continue;}
       Float_t dPhi6 = ftaNestedLoops[0]->GetAt(i6);
       Float_t dW6 = ftaNestedLoops[1]->GetAt(i6);
       for(int h=0; h<6; h++)
       {
        // fill cos, 6p, integreated: 
        fNestedLoopsPro[0][2][h][0]->Fill(0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2+dPhi3-dPhi4-dPhi5-dPhi6)),dW1*dW2*dW3*dW4*dW5*dW6);
        // fill cos, 6p, all harmonics, vs. M: 
        fNestedLoopsPro[0][2][h][1]->Fill(fnSelectedTracksEBE+0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2+dPhi3-dPhi4-dPhi5-dPhi6)),dW1*dW2*dW3*dW4*dW5*dW6);
       } // for(int h=0; h<6; h++)
      } // if(i6==i1||i6==i2||i6==i3||i6==i4||i6==i5){continue;}
     } // if(i5==i1||i5==i2||i5==i3||i5==i4){continue;}
    } // for(int i4=0; i4<fnSelectedTracksEBE; i4++)   
   } // for(int i3=0; i3<fnSelectedTracksEBE; i3++)
  } // for(int i2=0; i2<nTracks; ++i2)
 } // for(int i1=0; i1<nTracks; ++i1)

 // 8p:
 if(fnSelectedTracksEBE<8){return;}
 cout<<"      CalculateNestedLoops(void), 8-p correlations .... "<<endl;
 for(int i1=0; i1<fnSelectedTracksEBE; i1++)
 {
  Float_t dPhi1 = ftaNestedLoops[0]->GetAt(i1);
  Float_t dW1 = ftaNestedLoops[1]->GetAt(i1);
  for(int i2=0; i2<fnSelectedTracksEBE; i2++)
  {
   if(i2==i1){continue;}
   Float_t dPhi2 = ftaNestedLoops[0]->GetAt(i2);
   Float_t dW2 = ftaNestedLoops[1]->GetAt(i2);
   for(int i3=0; i3<fnSelectedTracksEBE; i3++)
   {
    if(i3==i1||i3==i2){continue;}
    Float_t dPhi3 = ftaNestedLoops[0]->GetAt(i3);
    Float_t dW3 = ftaNestedLoops[1]->GetAt(i3);
    for(int i4=0; i4<fnSelectedTracksEBE; i4++)
    {
     if(i4==i1||i4==i2||i4==i3){continue;}
     Float_t dPhi4 = ftaNestedLoops[0]->GetAt(i4);
     Float_t dW4 = ftaNestedLoops[1]->GetAt(i4);
     for(int i5=0; i5<fnSelectedTracksEBE; i5++)
     {
      if(i5==i1||i5==i2||i5==i3||i5==i4){continue;}
      Float_t dPhi5 = ftaNestedLoops[0]->GetAt(i5);
      Float_t dW5 = ftaNestedLoops[1]->GetAt(i5);
      for(int i6=0; i6<fnSelectedTracksEBE; i6++)
      {
       if(i6==i1||i6==i2||i6==i3||i6==i4||i6==i5){continue;}
       Float_t dPhi6 = ftaNestedLoops[0]->GetAt(i6);
       Float_t dW6 = ftaNestedLoops[1]->GetAt(i6);
       for(int i7=0; i7<fnSelectedTracksEBE; i7++)
       {
        if(i7==i1||i7==i2||i7==i3||i7==i4||i7==i5||i7==i6){continue;}
        Float_t dPhi7 = ftaNestedLoops[0]->GetAt(i7);
        Float_t dW7 = ftaNestedLoops[1]->GetAt(i7);
        for(int i8=0; i8<fnSelectedTracksEBE; i8++)
        {
         if(i8==i1||i8==i2||i8==i3||i8==i4||i8==i5||i8==i6||i8==i7){continue;}
         Float_t dPhi8 = ftaNestedLoops[0]->GetAt(i8);
         Float_t dW8 = ftaNestedLoops[1]->GetAt(i8);
         for(int h=0; h<6; h++)
         {
          // fill cos, 8p, integreated: 
          fNestedLoopsPro[0][3][h][0]->Fill(0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2+dPhi3+dPhi4-dPhi5-dPhi6-dPhi7-dPhi8)),dW1*dW2*dW3*dW4*dW5*dW6*dW7*dW8);
          // fill cos, 8p, all harmonics, vs. M: 
          fNestedLoopsPro[0][3][h][1]->Fill(fnSelectedTracksEBE+0.5,TMath::Cos((h+1.)*(dPhi1+dPhi2+dPhi3+dPhi4-dPhi5-dPhi6-dPhi7-dPhi8)),dW1*dW2*dW3*dW4*dW5*dW6*dW7*dW8);
         } // for(int h=0; h<6; h++)
        } // for(int i8=0; i8<fnSelectedTracksEBE; i8++)
       } // for(int i7=0; i7<fnSelectedTracksEBE; i7++)
      } // if(i6==i1||i6==i2||i6==i3||i6==i4||i6==i5){continue;}
     } // if(i5==i1||i5==i2||i5==i3||i5==i4){continue;}
    } // for(int i4=0; i4<fnSelectedTracksEBE; i4++)   
   } // for(int i3=0; i3<fnSelectedTracksEBE; i3++)
  } // for(int i2=0; i2<nTracks; ++i2)
 } // for(int i1=0; i1<nTracks; ++i1)

} // void FlowWithMultiparticleCorrelations::CalculateNestedLoops(void) 

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::ComparisonNestedLoopsVsCorrelations(void)
{
 // Make a ratio fNestedLoopsPro[....]/fCorrelationsPro[....]. If results are the same, these ratios must be 1.

 // a) Integrated comparison;
 // b) Comparison vs. multiplicity;
 // c) Comparison vs. centrality.

 cout<<":: FlowWithMultiparticleCorrelations::ComparisonNestedLoopsVsCorrelations(void)"<<endl; 

 Int_t nBinsQV = -44;
 Int_t nBinsNL = -44;
 Float_t valueQV = 0.;
 Float_t valueNL = 0.;


 // a) Integrated comparison:
 nBinsQV = fCorrelationsPro[0][0][0][0]->GetNbinsX();
 nBinsNL = fNestedLoopsPro[0][0][0][0]->GetNbinsX();
 if(nBinsQV != nBinsNL){cout<<__LINE__<<endl; exit(0);}
 cout<<endl;
 cout<<"   [0] : integrated"<<endl;
 for(Int_t o=0;o<4;o++)
 {
  cout<<Form("   ==== %d-particle correlations ====",2*(o+1))<<endl;
  for(Int_t h=0;h<6;h++)
  {
   for(Int_t b=1;b<=nBinsQV;b++)
   {
    if(fCorrelationsPro[0][o][h][0]){valueQV = fCorrelationsPro[0][o][h][0]->GetBinContent(b);}
    if(fNestedLoopsPro[0][o][h][0]){valueNL = fNestedLoopsPro[0][o][h][0]->GetBinContent(b);}
    if(TMath::Abs(valueQV)>0. && TMath::Abs(valueNL)>0.)
    {
     cout<<"   Q-vectors:    "<<valueQV<<endl; 
     cout<<"   Nested loops: "<<valueNL<<endl; 
     if(TMath::Abs(valueQV-valueNL)>1.e-5)
     {          
      cout<<Form("[%d][%d][%d][%d]",0,o,h,0)<<endl; cout<<__LINE__<<endl; exit(0);
     }
    } // if(TMath::Abs(valueQV)>0. && TMath::Abs(valueNL)>0.)
   } // for(Int_t b=1;b<=nBinsQV;b++) 
  } // for(Int_t h=0;h<6;h++)
  cout<<endl;
 } // for(Int_t o=0;o<4;o++) 

 cout<<endl;

 // b) Comparison vs. multiplicity:
 nBinsQV = fCorrelationsPro[0][0][0][1]->GetNbinsX();
 nBinsNL = fNestedLoopsPro[0][0][0][1]->GetNbinsX();
 if(nBinsQV != nBinsNL){cout<<__LINE__<<endl; exit(0);}
 cout<<endl;
 cout<<"   [1] : vs. multiplicity"<<endl;
 for(Int_t o=0;o<4;o++)
 {
  cout<<Form("   ==== %d-particle correlations ====",2*(o+1))<<endl;
  for(Int_t h=0;h<6;h++)
  {
   for(Int_t b=1;b<=nBinsQV;b++)
   {
    if(fCorrelationsPro[0][o][h][1]){valueQV = fCorrelationsPro[0][o][h][1]->GetBinContent(b);}
    if(fNestedLoopsPro[0][o][h][1]){valueNL = fNestedLoopsPro[0][o][h][1]->GetBinContent(b);}
    if(TMath::Abs(valueQV)>0. && TMath::Abs(valueNL)>0.)
    {
     cout<<"   Q-vectors:    "<<valueQV<<endl; 
     cout<<"   Nested loops: "<<valueNL<<endl; 
     if(TMath::Abs(valueQV-valueNL)>1.e-5)
     {          
      cout<<Form("[%d][%d][%d][%d]",0,o,h,1)<<endl; cout<<__LINE__<<endl; exit(0);
     }
    } // if(TMath::Abs(valueQV)>0. && TMath::Abs(valueNL)>0.)
   } // for(Int_t b=1;b<=nBinsQV;b++) 
  } // for(Int_t h=0;h<6;h++)
  cout<<endl;
 } // for(Int_t o=0;o<4;o++) 

 cout<<endl;

 // c) Comparison vs. centrality:
 // nBinsQV = fCorrelationsPro[0][0][0][2]->GetNbinsX();
 // nBinsNL = fNestedLoopsPro[0][0][0][2]->GetNbinsX();
 // TBI_20209627 add support eventually

} // void FlowWithMultiparticleCorrelations::ComparisonNestedLoopsVsCorrelations(void)

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::ResetEventByEventQuantities(void)
{
 // Reset all event-by-event quantities.

 // a) Reset counter for selected tracks;
 // b) Reset containers for nested loops;
 // c) Reset Q-vector.

 // a) Reset counter for selected tracks:
 fnSelectedTracksEBE = 0;

 // b) Reset containers for nested loops:
 if(ftaNestedLoops[0]){ftaNestedLoops[0]->Reset();} 
 if(ftaNestedLoops[1]){ftaNestedLoops[1]->Reset();} 

 // c) Reset Q-vector:
 if(fCalculateQvector)
 {
  for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++)
  {
   for(Int_t wp=0;wp<fMaxCorrelator+1;wp++) // weight power
   {
    fQvector[h][wp] = TComplex(0.,0.); 
   }
  }
 } // if(fCalculateQvector)

} // void FlowWithMultiparticleCorrelations::ResetEventByEventQuantities(void)

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::BookAllOnTheFlyObjects()
{
 // Book all objects for Toy Monte Carlo (flow analysis 'on-the-fly') in this method.

 // a) Determine seed for gRandom;
 // b) Book Fourier series to simulate distributions with flow;
 // c) Book default distributions;
 // d) Book non-default distributions.

 cout<<":: FlowWithMultiparticleCorrelations::BookAllOnTheFlyObjects()"<<endl; 

 // a) Determine seed for gRandom:
 delete gRandom;
 gRandom = new TRandom3(fRandomSeed); // if uiSeed is 0, the seed is determined uniquely in space and time via TUUID

 // b) Book Fourier series to simulate distributions with flow:
 Float_t dPhiMin = -TMath::Pi(); 
 Float_t dPhiMax = TMath::Pi();
 fPhiPDF = new TF1("fPhiPDF","1+2.*[1]*TMath::Cos(x-[0])+2.*[2]*TMath::Cos(2.*(x-[0]))+2.*[3]*TMath::Cos(3.*(x-[0]))+2.*[4]*TMath::Cos(4.*(x-[0]))+2.*[5]*TMath::Cos(5.*(x-[0]))+2.*[6]*TMath::Cos(6.*(x-[0]))",dPhiMin,dPhiMax);
 fPhiPDF->SetParName(0,"Reaction Plane");
 fPhiPDF->SetParameter(0,0.);
 fPhiPDF->SetParName(1,"Directed Flow (v1)"); 
 fPhiPDF->SetParameter(1,fV1);
 fPhiPDF->SetParName(2,"Elliptic Flow (v2)");
 fPhiPDF->SetParameter(2,fV2);
 fPhiPDF->SetParName(3,"Triangular Flow (v3)");
 fPhiPDF->SetParameter(3,fV3);
 fPhiPDF->SetParName(4,"Quadrangular Flow (v4)");
 fPhiPDF->SetParameter(4,fV4);
 fPhiPDF->SetParName(5,"Pentagonal Flow (v5)");
 fPhiPDF->SetParameter(5,fV5);
 fPhiPDF->SetParName(6,"Hexagonal Flow (v6)");
 fPhiPDF->SetParameter(6,fV6);

 // c) Book default distributions:
 if(fUseDefaultPDFs)
 {
  cout<<"   Using default hardwired PDFs."<<endl;

  // Define default pt distribution:
  Float_t dPtMin = 0.; 
  Float_t dPtMax = 10.; 
  fPtPDF = new TF1("fPtPDF","x*TMath::Exp(-pow([0]*[0]+x*x,0.5)/[1])",dPtMin,dPtMax); // hardwired is Boltzmann distribution  
  fPtPDF->SetParName(0,"Mass");
  fPtPDF->SetParameter(0,fMass);
  fPtPDF->SetParName(1,"Temperature");
  fPtPDF->SetParameter(1,fTemperature);
  fPtPDF->SetTitle("Boltzmann Distribution: f(p_{t}) = p_{t}exp[-(m^{2}+p_{t}^{2})^{1/2}/T];p_{t};f(p_{t})");

  // Define default eta distribution:
  Float_t dEtaMin = -2.; 
  Float_t dEtaMax = 10.; 
  fEtaPDF = new TF1("fEtaPDF","1",dEtaMin,dEtaMax); // hardwired is uniform distribution  

  // Define default multiplicity distribution:
  fMultiplicityPDF = new TF1("fMultPDF","1",fMultRangePDF[0],fMultRangePDF[1]); // hardwired is uniform distribution, use void SetMultiplicityRange(Float_t min, Float_t max) for ranges    

  // Define default detector acceptance:
  Float_t dFirstSector[2] = {-(3./4.)*TMath::Pi(),-(1./4.)*TMath::Pi()}; // first sector is defined as [-3Pi/4,Pi/4] TBI_20200621 promote also to data members, and provide setters
  Float_t dSecondSector[2] = {(1./3.)*TMath::Pi(),(2./3.)*TMath::Pi()}; // second sector is defined as [Pi/3,2Pi/3] TBI_20200621 promote also to data members, and provide setters
  fDetectorAcceptancePDF = new TF1("fDetectorAcceptancePDF","1.-(x>=[0])*(1.-[4]) + (x>=[1])*(1.-[4]) - (x>=[2])*(1.-[5]) + (x>=[3])*(1.-[5]) ",-TMath::Pi(),TMath::Pi());  
  fDetectorAcceptancePDF->SetParameter(0,dFirstSector[0]);
  fDetectorAcceptancePDF->SetParameter(1,dFirstSector[1]);
  fDetectorAcceptancePDF->SetParameter(2,dSecondSector[0]);
  fDetectorAcceptancePDF->SetParameter(3,dSecondSector[1]);
  fDetectorAcceptancePDF->SetParameter(4,fProbability[0]);
  fDetectorAcceptancePDF->SetParameter(5,fProbability[1]);

 } // if(fUseDefaultPDFs)

 // d) Book non-default distributions:
 if(!fUseDefaultPDFs)
 {
  cout<<"   Using non-default PDFs supplied via the external file."<<endl;

  fOnTheFlyList->Add(fNonDefaultPDFs[0]);
  fOnTheFlyList->Add(fNonDefaultPDFs[1]);
  fOnTheFlyList->Add(fNonDefaultPDFs[2]);
  fOnTheFlyList->Add(fNonDefaultPDFs[3]);

 } // if(!fUseDefaultPDFs)

 return;

} // void FlowWithMultiparticleCorrelations::BookAllOnTheFlyObjects()

//=======================================================================================================================

void FlowWithMultiparticleCorrelations::CalculateOnTheFly(void)
{
 // Toy Monte Carlo (flow analysis 'on-the-fly'). 

 // a) Reset event-by-event quantities;
 // b) Make event on the fly;
 // c) Calculate correlations from Q-vectors;
 // d) Calculate correlations from nested loops.

 cout<<":: FlowWithMultiparticleCorrelations::CalculateOnTheFly(void)"<<endl; 

 // a) Reset event-by-event quantities:
 this->ResetEventByEventQuantities();

 // b) Make event on the fly:
 //    Determine the multiplicity of an event:
 Int_t nMult = 0;
 if(fUseDefaultPDFs)
 {
  nMult = (Int_t)fMultiplicityPDF->GetRandom(); // sampling from TF1
 } 
 else
 {
  nMult = (Int_t)fNonDefaultPDFs[0]->GetRandom(); // sampling from TH1F
 }
 if(nMult < fMultiplicityCuts[0] || nMult > fMultiplicityCuts[1]){return;}
 if(fMultiplicity[0]){fMultiplicity[0]->Fill(nMult);}

 //    Determine the reaction plane of an event:
 Float_t fReactionPlane = gRandom->Uniform(0.,TMath::TwoPi());
 fImpactParameter[0][1]->Fill(fReactionPlane); // orientation of impact parameter [before][sim]
 fPhiPDF->SetParameter(0,fReactionPlane);

 //    Sample the particles and fill Q-vectors:
 Float_t dPhi = 0., wPhi = 1.; // azimuthal angle and corresponding phi weight
 Float_t dPt = 0., wPt = 1.; // transverse momentum and corresponding pT weight
 Float_t dEta = 0., wEta = 1.; // pseudorapidity and corresponding eta weight
 //Float_t dRapidity = 0., wRapidty = 1.; // rapidity and corresponding rapidity weight TBI_20200612 enable eventually
 Float_t wToPowerP = 1.; // weight raised to power p
 for(Int_t p=0;p<nMult;p++)
 {
  dPhi = fPhiPDF->GetRandom();
  if(!AcceptPhi(dPhi)){continue;}
  if(fUseDefaultPDFs)
  {
   dPt  = fPtPDF->GetRandom(); // sampling from TF1
   dEta = fEtaPDF->GetRandom(); // sampling from TF1
  }
  else
  {
   dPt  = fNonDefaultPDFs[1]->GetRandom(); // sampling from TH1F
   dEta = fNonDefaultPDFs[2]->GetRandom(); // sampling from TH1F
  }

  // Fill control histos:
  if(fKinematics[0][2][0]){fKinematics[0][2][0]->Fill(dPhi);}
  if(fKinematics[0][2][1]){fKinematics[0][2][1]->Fill(dPt);}
  if(fKinematics[0][2][2]){fKinematics[0][2][2]->Fill(dEta);}

  if(fCalculateQvector || fCalculateNestedLoops)
  {
   // Access:
   if(fUseWeights[0]){wPhi = Weight(dPhi,"phi");} // corresponding phi weight
   if(fUseWeights[1]){wPt  = Weight(dPt,"pt");} // corresponding pt weight
   if(fUseWeights[2]){wEta = Weight(dEta,"eta");} // corresponding eta weight
   //if(fUseWeights[3]){wRapidty = Weight(dRapidity,"rapidity");} // corresponding rapidity weight TBI_20200612 enable eventually
   // Fill containers for nested loops:
   if(ftaNestedLoops[0]){ftaNestedLoops[0]->AddAt(dPhi,fnSelectedTracksEBE);} 
   if(ftaNestedLoops[1]){ftaNestedLoops[1]->AddAt(wPhi*wPt*wEta,fnSelectedTracksEBE);} 
   // Finally, calculate Q-vectors:
   for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++)
   {
    for(Int_t wp=0;wp<fMaxCorrelator+1;wp++) // weight power
    {
    if(fUseWeights[0]||fUseWeights[1]||fUseWeights[2]||fUseWeights[3]){wToPowerP = pow(wPhi*wPt*wEta,wp);} 
    //if(fUseWeights[0]||fUseWeights[1]||fUseWeights[2]||fUseWeights[3]){wToPowerP = pow(wPhi*wPt*wEta*wRapidity,wp);} // TBI_20200612 same as above, just taking also wRapidity
     fQvector[h][wp] += TComplex(wToPowerP*TMath::Cos(h*dPhi),wToPowerP*TMath::Sin(h*dPhi));
    } // for(Int_t wp=0;wp<fMaxCorrelator+1;wp++)
   } // for(Int_t h=0;h<fMaxHarmonic*fMaxCorrelator+1;h++)   
  } // if(fCalculateQvector || fCalculateNestedLoops)
  fnSelectedTracksEBE++;
 } // for(Int_t p=0;p<nMult;p++)

 // c) Calculate correlations from Q-vectors:
 if(fCalculateCorrelations)
 {
  this->CalculateCorrelations();
 }

 // d) Calculate correlations from nested loops:
 if(fCalculateNestedLoops)
 {
  this->CalculateNestedLoops();
 }

 return;

} // void FlowWithMultiparticleCorrelations::CalculateOnTheFly(void)

//=======================================================================================================================

Bool_t FlowWithMultiparticleCorrelations::GetNonDefaultPDFs(const char *filePath)
{
 // Get non-default PDFs for multiplicity, pt, eta and azimuthal acceptance. This is relevant only for Toy Monte Carlo (flow analysis 'on-the-fly').
 // Remark 0: This method expects histograms with the following names in file at filePath:
 // a) 'multiplicityPDF'
 // b) 'ptPDF'
 // c) 'etaPDF'
 // d) 'detectorAcceptancePDF'
 // Remark 1: Use the macro makeNonDefaultPDFs.C to transfer and rename histos from "AnalysisResults.root" to "NonDefaultPDFs.root"

 cout<<":: FlowWithMultiparticleCorrelations::GetNonDefaultPDFs(const char *filePath)"<<endl; 
    
 // a) Set modus operandi to non-default p.d.f.s;
 // b) Check if the external ROOT file exists at specified path;
 // c) Access the external ROOT file and fetch the desired non-default PDFs.

 // a) Set modus operandi to non-default p.d.f.s:
 fUseDefaultPDFs = kFALSE;

 // b) Check if the external ROOT file exists at specified path:
 if(gSystem->AccessPathName(filePath,kFileExists))
 {
  cout<<Form("if(gSystem->AccessPathName(filePath,kFileExists)), filePath = %s",filePath)<<endl; exit(0);
 }

 // c) Access the external ROOT file and fetch the desired non-default PDFs:
 TFile *file = TFile::Open(filePath,"READ");
 if(!file) exit(0);
 //    multiplicity:
 fNonDefaultPDFs[0] = (TH1F*)(file->Get("multiplicityPDF"));
 if(!fNonDefaultPDFs[0]){cout<<__LINE__<<endl; exit(0);}
 fNonDefaultPDFs[0]->SetDirectory(0);
 fNonDefaultPDFs[0]->SetTitle("input p.d.f. for multiplicity");

 //    pt:
 fNonDefaultPDFs[1] = (TH1F*)(file->Get("ptPDF"));
 if(!fNonDefaultPDFs[1]){cout<<__LINE__<<endl; exit(0);}
 fNonDefaultPDFs[1]->SetDirectory(0);
 fNonDefaultPDFs[1]->SetTitle("input p.d.f. for p_{t}");

 //    eta:
 fNonDefaultPDFs[2] = (TH1F*)(file->Get("etaPDF"));
 if(!fNonDefaultPDFs[2]){cout<<__LINE__<<endl; exit(0);}
 fNonDefaultPDFs[2]->SetDirectory(0);
 fNonDefaultPDFs[2]->SetTitle("input p.d.f. for #eta");

 //    detector's acceptance:
 fNonDefaultPDFs[3] = (TH1F*)(file->Get("detectorAcceptancePDF"));
 if(!fNonDefaultPDFs[3]){cout<<__LINE__<<endl; exit(0);}
 fNonDefaultPDFs[3]->SetDirectory(0);
 fNonDefaultPDFs[3]->SetTitle("normalized input p.d.f. for detector's acceptance:"); 
 Double_t norm = 1;
 fNonDefaultPDFs[3]->Scale(norm/fNonDefaultPDFs[3]->Integral(), "width");
 if(TMath::Abs(fNonDefaultPDFs[3]->Integral("width")-1.)>1.e-7){cout<<__LINE__<<endl; exit(0);}
 //cout<<fNonDefaultPDFs[3]->Integral("width")<<endl; exit(0);

 return kTRUE;
 
} // Bool_t FlowWithMultiparticleCorrelations::GetNonDefaultPDFs(const char *filePath)

//=======================================================================================================================

Bool_t FlowWithMultiparticleCorrelations::AcceptPhi(const Float_t &value)
{
 // Accept or not this azimuthal angle. TBI_20200620 generalize to other kinematic variables, in the same spirit as Weights(...)

 Bool_t bAccept = kTRUE;

 Float_t acceptanceProbability = 1.; 

 if(fUseDefaultPDFs)
 {
  // Get max entry in the p.d.f, and scale probabilities relative to it:
  Float_t max = fDetectorAcceptancePDF->GetMaximum(-TMath::Pi(),TMath::Pi()); // TBI_20200620 promote this to data member, as this is always, there is a huge loss of efficieny here
  Float_t correspondingAcceptance = fDetectorAcceptancePDF->Eval(value); // TBI_20200620 validate this line again
  acceptanceProbability = 1.-(max-correspondingAcceptance)/max;
 }

 if(!fUseDefaultPDFs)
 {
  // Get max entry in the acceptance histogram, and scale probabilities relative to it:
  Float_t max = fNonDefaultPDFs[3]->GetMaximum(); // TBI_20200620 promote this to data member, as this is always, there is a huge loss of efficieny here

  // TBI 
  Float_t correspondingAcceptance = fNonDefaultPDFs[3]->GetBinContent(fNonDefaultPDFs[3]->FindBin(value)); // TBI_20200620 validate this line again

  // Probability to accept:
  acceptanceProbability = 1.-(max-correspondingAcceptance)/max;

 }

 // Accept or not:
 (gRandom->Uniform(0,1) < acceptanceProbability) ? bAccept = kTRUE : bAccept = kFALSE;

 return bAccept;
 
} // Bool_t FlowWithMultiparticleCorrelations::AcceptPhi(Float_t dPhi) 

//=======================================================================================================================



